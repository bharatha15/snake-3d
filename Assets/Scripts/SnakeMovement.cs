using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class SnakeMovement : MonoBehaviour
{
    #region Serialized Private Fields
    [Tooltip("Snake moving speed")]
    [SerializeField] private float speed = 2f;
    [Header("Scripts")]
    [Tooltip("Score Board Script")]
    [SerializeField] private ScoreBoard scoreBoard;
    [Tooltip("Ground Handler Script")]
    [SerializeField] private GroundHandler groundHandler;
    #endregion

    #region Private Fields
    private Animator snakeAnim;
    private static int greenTileCount = 0;
    private int pizzaEatenCount = 0;
    #endregion

    #region Public 
    public static SnakeMovement Instance
    {
        get; private set;
    }
    #endregion

    #region Monobehaviour Callbacks

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        snakeAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        SnakeMove();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Material mat;

        /* If snake Collides with the Tile, then Tile turns to green color. 
         */
        if (collision.collider.transform.tag == "tile")
        {
            mat = collision.transform.GetComponent<Renderer>().material;
            mat.color = ApplicationHandler.GreenMaterial;

            bool isGreen = groundHandler.IsAllTilesGreen(collision.collider.transform);

        }

        /* Score will increase by +1, when the snake eats the Pizza.
         * Then the pizza will be deactivated.
         */
        if(collision.collider.transform.tag == "pizza")
        {
            pizzaEatenCount += 1;
            ApplicationHandler.Score = pizzaEatenCount;
            collision.collider.transform.gameObject.SetActive(false);
            scoreBoard.SetScore(ApplicationHandler.Score);
        }

        /*Snake will bounce back little bit when it collides with the walls.
         */
        if(collision.collider.transform.tag == "wall")
        {
            Debug.Log("Snake Hitting the wall.");
            BounceBack();
        }

    }
    #endregion

    #region Private Methods
    /*
     * Snake Moving in the forward direction.
     */
    void SnakeMove()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    /*
     Stop the Right and Left turn animation after few seonds
     */
    IEnumerator StopAnimation()
    {
        yield return new WaitForSeconds(0.2f);
        snakeAnim.SetBool("right", false);
        snakeAnim.SetBool("left", false);
    }
    #endregion


    #region Public Methods

    /*
     * Snake turning right for about +45degrees
     */
    public void SnakeRightTurn()
    {
        
        snakeAnim.SetBool("right", true);
        transform.Rotate(new Vector3(0f, 45f, 0f));
        StartCoroutine(StopAnimation());
    }

    /*
    * Snake turning left for about -45degrees
    */
    public void SnakeLeftTurn()
    {
        snakeAnim.SetBool("left", true);
        transform.Rotate(new Vector3(0f, -45f, 0f));
        StartCoroutine(StopAnimation());
    }

    /*
     * Snake Bounce backs when hit the wall.
     */
    public void BounceBack()
    {
        transform.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0,0,-1f), ForceMode.Impulse);

    }

    #endregion


}
