using UnityEngine;

public class ApplicationHandler : ScriptableObject
{
    /*
     * Enumerated Scene Names
     */
    public enum Scene
    {
        Start,
        Game01,
        End
    }

    /* Green Colour for the Tile */
    public static Color GreenMaterial = Color.green;

    /* Total number of tiles instantiated dynamically.*/
    public static int TilesCount;

    /* Total Score after eating pizza */
    public static int Score;

    /* Check if Application is Paused, default value is false. */
    public static bool ApplicationPaused = false;

}
