using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    #region
    [Tooltip("TMP Text")]
    [SerializeField] private TMP_Text scoreValue;
    #endregion

    #region Private Fields

    #endregion

    #region Public
    public static ScoreBoard Instance { get; private set; }
    #endregion

    #region Monobehaviour callbacks

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
        ResetScore();
        GetScore();
        SetScore(GetScore());
        
    }


    #endregion

    #region Private Methods

    /*
     * Update the score by +1 when snake eats the pizza
     */
    public void SetScore(int score)
    {
        scoreValue.text = score.ToString();
    }

    /*
     * Reset the score of snake to zero.
     */
    void ResetScore()
    {
        scoreValue.text = "0";
    }

    /*
     * Returns the score
     */
    int GetScore()
    {
        return ApplicationHandler.Score;
    }
    #endregion
}
