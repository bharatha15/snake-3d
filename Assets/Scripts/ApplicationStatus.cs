using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ApplicationStatus : MonoBehaviour
{
    #region Serialized Private Fields
    #endregion

    #region Monobehaviour Callbacks

    #endregion

    #region Public Methods

    /* 
     * Start the Application by loading the new game scene
     */
    public void StartApplication()
    {
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(ApplicationHandler.Scene.Game01.ToString());
    }



    /*
     * Pause the Application
     */
    public void PauseApplication()
    {
        Time.timeScale = 0.0f;
    }

    /*
     * Resume Application
     */
    public void ResumeApplication()
    {
        Time.timeScale = 1.0f;
    }


    /*
     Quit the application
     */
    public void EndApplication()
    {
        Application.Quit();
    }
    #endregion
}
