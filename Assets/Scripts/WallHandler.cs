using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHandler : MonoBehaviour
{
    [Header("Gameobjects/Prefabs")]
    [SerializeField] private GameObject wall01;
    [SerializeField] private GameObject wall02;

    private GroundHandler groundHandler;

    // Start is called before the first frame update
    void Start()
    {
        groundHandler = GetComponent<GroundHandler>();
        InitializeWalls();
    }


    /*
     * Initialize the 4 walls around the Ground sides.
     * These walls will be instantiated based on the Width and Length of the Ground. 
     */
    void InitializeWalls()
    {


        for (int i = 1; i < groundHandler.Width; i++)
        {
            GameObject obj = Instantiate(wall01, 
                new Vector3(wall01.transform.position.x + i, wall01.transform.position.y, wall01.transform.position.z), 
                Quaternion.identity);
            obj.transform.SetParent(transform);
        }

        for(int i=1; i<groundHandler.Length; i++)
        {
            GameObject obj = Instantiate(wall02, 
                new Vector3(wall02.transform.position.x, wall02.transform.position.y, wall02.transform.position.z+i), 
                Quaternion.identity);
            obj.transform.SetParent(transform);
        }

        for(int i=0; i<groundHandler.Width; i++)
        {
            GameObject obj = Instantiate(wall01, 
                new Vector3(wall01.transform.position.x + i, wall01.transform.position.y, wall01.transform.position.z+groundHandler.Length+1), 
                Quaternion.identity);
            obj.transform.SetParent(transform);
        }

        for (int i = 0; i < groundHandler.Length; i++)
        {
            GameObject obj = Instantiate(wall02,
                new Vector3(wall02.transform.position.x+ groundHandler.Width +1, wall02.transform.position.y, wall02.transform.position.z+i), 
                Quaternion.identity);
            obj.transform.SetParent(transform);
        }
    }
}
