using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundHandler : MonoBehaviour
{
    #region Serialized Private Fields
    [Header("Grid Size")]
    [Tooltip("Width of the Play Ground in units(X direction). Input should be in Integer even values")]
    [SerializeField] private int width = 0;
    [Tooltip("Length of the Play Ground in units(Y direction). Input should be in Integer even values")]
    [SerializeField] private int length = 0;

    [Header("Prefab/Gameobject")]
    [Tooltip("Tile Prefab")]
    [SerializeField] private GameObject tile;
    
    [Header("Scripts")]
    [SerializeField] private ApplicationStatus applicationStatus;
    #endregion

    #region Private Fields
    int count = 0;

    #endregion

    #region Public Fields
    public static List<GameObject> tileList = new List<GameObject>();
    #endregion

    #region Public Properties
    /* 
     * default is false.
     * returns true if all the tiles turn green.
     */
    public bool AllTilesGreen { set; get; }
    public int Width { set { width = value; } get { return width; } }
    public int Length { set { length = value; } get { return length; } }

    /* contains tiles with having unique name */
    public HashSet<string> tileSetName = new HashSet<string>();
    public HashSet<string> TileSetName
    {
        get { return tileSetName; }
    }
    #endregion


    #region Monobehaviour Callbacks
    void Start()
    {
        
        AllTilesGreen = false;
        TileInitialise();
        
    }

    
    void Update()
    {
        
    }
    #endregion

    #region Private Methods

    /*
     Instanitates/Initializes the files, based on width and height inputs.
     */
    void TileInitialise()
    {
        tile.SetActive(true);
        tileList.Add(tile);
        tile.tag = "tile";

        float xPose = tile.transform.position.x;
        float yPose = tile.transform.position.y;
        float zPose = tile.transform.position.z;

        // X Axis Initialisation(Width Initialisation)
        for (int i = 1; i < Width; i++)
        {
            GameObject tileObj = Instantiate(tile);
            tileObj.transform.position = new Vector3(xPose + i, yPose, zPose);
            tileObj.tag = "tile";
            tileObj.name = "Width" + i;     // Each tile will have an unique name.
            tileSetName.Add(tileObj.name);  // Tiles will be added to HashSet
            tileObj.transform.SetParent(transform);
            tileList.Add(tileObj);


        }

        // Y Axis Initialisation(Length Initialisation)
        for (int j=1; j<Length; j++)
        {
            foreach (var obj in tileList)
            {
                GameObject gameObject = Instantiate(obj);
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + j);
                gameObject.tag = "tile";
                gameObject.name = "Length" + obj.name + j;  // Each tile will have an unique name.
                tileSetName.Add(gameObject.name);           // Tiles will be added to HashSet
                gameObject.transform.SetParent(transform);
            }
        }

        StartCoroutine(TotalTilesCount());

    }

    /*
     * Total number of tiles
     */
    IEnumerator TotalTilesCount()
    {
        yield return new WaitForSeconds(0.5f);
        ApplicationHandler.TilesCount = transform.childCount-1; // Subtracting the pizza Gameobject from the List of tile Gameobjects.
    }

    /*
     * Enable the Pizza Handler Script
     
    void EnablePizzaHandlerScript()
    {
        GetComponent<PizzaHandler>().enabled = true;
    }
    */

    #endregion

    #region Public Methods
    /* 
     returns true, if all the Tiles has been Turned to green.
     So when all of them turns green and Set counts to zero, the application gets paused and loads 
     the End Scene.
     */
    public bool IsAllTilesGreen(Transform obj)
    {
        bool out_ = false;
        
        if (TileSetName.Contains(obj.name))
        {
            TileSetName.Remove(obj.name);
            if(TileSetName.Count == 0)
            {
                out_ = true;
                AllTilesGreen = out_;
                applicationStatus.PauseApplication();
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(ApplicationHandler.Scene.End.ToString()); 

            }
            else
            {
                out_ = false;
            }
        }return out_;
    }
    #endregion

}
