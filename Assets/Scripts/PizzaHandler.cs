using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using UnityEngine;

public class PizzaHandler : MonoBehaviour
{

    #region
    [Tooltip("Pizza Game Object")]
    [SerializeField] private GameObject pizza;
    [Tooltip("Pizza active time in seconds")]
    [SerializeField] private float pizzaActiveTime = 5f;
    #endregion

    #region Private Fields
    private float pizzaYoffset = 0.02f;
    private GroundHandler groundHandler;
    #endregion


    #region Monobehaviour Callbacks
    [System.Obsolete]
    void Start()
    {
        groundHandler = GetComponent<GroundHandler>(); 
        PizzaInit();
    }

    void Update()
    {
        
    }

    private void OnEnable()
    {

    }

    [System.Obsolete]
    private void OnDisable()
    {
      
        
    }

    #endregion


    #region
    /*
     * Initialising the Pizza.
     */
    [System.Obsolete]
    void PizzaInit()
    {
        if (!groundHandler.AllTilesGreen)
        {
            pizza.SetActive(true);
            string randomTileName = RandomNonGreenTile();
            PizzaPosition(GameObject.Find("Ground/" + randomTileName)); // Finding Gameobject from the Hierarchy
            StartCoroutine(PizzaTimeOut());
        }

    }


    /*
     * Set the Position of the Pizza.
     */
    [System.Obsolete]
    Vector3 PizzaPosition(GameObject obj)
    {
        GameObject firstObj = obj;
        float x = firstObj.transform.position.x;
        float y = firstObj.transform.position.y + firstObj.transform.localScale.y/2  + pizzaYoffset;
        float z = firstObj.transform.position.z;
        pizza.transform.position = new Vector3(x, y, z);
        return pizza.transform.position;
    }



    /*
     * Deactivate Pizza after certain Time. 
     */
    [System.Obsolete]
    IEnumerator PizzaTimeOut()
    {
        yield return new WaitForSeconds(pizzaActiveTime);
        pizza.SetActive(false);
        PizzaInit();

    }

    /*
     * Return Non Green Random Tile.
     */
    string RandomNonGreenTile()
    {
        int initCount = 0;
        List<string> tileList = groundHandler.TileSetName.ToList();
        int random = (int)Random.Range(initCount, tileList.Count);
        return tileList[random];
    }
    #endregion

}
