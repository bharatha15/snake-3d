﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityMeshSimplifier.BlendShape::.ctor(System.String,UnityMeshSimplifier.BlendShapeFrame[])
extern void BlendShape__ctor_m3871790C2786D76805CDC5AD29DB19DA81B5AC4A (void);
// 0x00000002 System.Void UnityMeshSimplifier.BlendShapeFrame::.ctor(System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3[])
extern void BlendShapeFrame__ctor_m45CEC892AD8D65A153FAAC0206A9469C0AF98145 (void);
// 0x00000003 UnityEngine.Renderer[] UnityMeshSimplifier.LODBackupComponent::get_OriginalRenderers()
extern void LODBackupComponent_get_OriginalRenderers_m484DA92D2B4A199D50B8CDB65BDC0356256FA55D (void);
// 0x00000004 System.Void UnityMeshSimplifier.LODBackupComponent::set_OriginalRenderers(UnityEngine.Renderer[])
extern void LODBackupComponent_set_OriginalRenderers_m6385D2A75FD6617736FE4EE05968BFF2E3DD5262 (void);
// 0x00000005 System.Void UnityMeshSimplifier.LODBackupComponent::.ctor()
extern void LODBackupComponent__ctor_m981426563EF27695D5DA32DBF8F52CBAF7DF2544 (void);
// 0x00000006 UnityEngine.LODFadeMode UnityMeshSimplifier.LODGeneratorHelper::get_FadeMode()
extern void LODGeneratorHelper_get_FadeMode_m5FD9BC3B1F350A0979CFC3639584C81C0904651E (void);
// 0x00000007 System.Void UnityMeshSimplifier.LODGeneratorHelper::set_FadeMode(UnityEngine.LODFadeMode)
extern void LODGeneratorHelper_set_FadeMode_mA95C65DACBB0FFE92756CF67551E19D56683D1AD (void);
// 0x00000008 System.Boolean UnityMeshSimplifier.LODGeneratorHelper::get_AnimateCrossFading()
extern void LODGeneratorHelper_get_AnimateCrossFading_m61A39A584C4B01FEAAEF6B748723704ACD257019 (void);
// 0x00000009 System.Void UnityMeshSimplifier.LODGeneratorHelper::set_AnimateCrossFading(System.Boolean)
extern void LODGeneratorHelper_set_AnimateCrossFading_m6FFC768B0A25770DCF7A230F9DC7546D4CEF4BA4 (void);
// 0x0000000A System.Boolean UnityMeshSimplifier.LODGeneratorHelper::get_AutoCollectRenderers()
extern void LODGeneratorHelper_get_AutoCollectRenderers_m6802F78D2EF57341C33E8730619E5731AAB18FA1 (void);
// 0x0000000B System.Void UnityMeshSimplifier.LODGeneratorHelper::set_AutoCollectRenderers(System.Boolean)
extern void LODGeneratorHelper_set_AutoCollectRenderers_m24391B8D5811A944DD4C9B8C0FB613B1B39F5D5A (void);
// 0x0000000C UnityMeshSimplifier.SimplificationOptions UnityMeshSimplifier.LODGeneratorHelper::get_SimplificationOptions()
extern void LODGeneratorHelper_get_SimplificationOptions_mE40EE26172A078EDBB656095F65BF97B1B823A92 (void);
// 0x0000000D System.Void UnityMeshSimplifier.LODGeneratorHelper::set_SimplificationOptions(UnityMeshSimplifier.SimplificationOptions)
extern void LODGeneratorHelper_set_SimplificationOptions_m0B31AAC1C138D96089624155928CDA15A94EAB44 (void);
// 0x0000000E System.String UnityMeshSimplifier.LODGeneratorHelper::get_SaveAssetsPath()
extern void LODGeneratorHelper_get_SaveAssetsPath_m65B35A8FB8328C25DB5BE7EB4D47244822C8C009 (void);
// 0x0000000F System.Void UnityMeshSimplifier.LODGeneratorHelper::set_SaveAssetsPath(System.String)
extern void LODGeneratorHelper_set_SaveAssetsPath_m0C025B183274E15B29D3FD73DBA9A3B5800C5C14 (void);
// 0x00000010 UnityMeshSimplifier.LODLevel[] UnityMeshSimplifier.LODGeneratorHelper::get_Levels()
extern void LODGeneratorHelper_get_Levels_mEA112741384066B9BA84DF4EC5F264C2B5FA8C79 (void);
// 0x00000011 System.Void UnityMeshSimplifier.LODGeneratorHelper::set_Levels(UnityMeshSimplifier.LODLevel[])
extern void LODGeneratorHelper_set_Levels_m5E7E0026E04F089507663F8B7624E95A0AEBF182 (void);
// 0x00000012 System.Boolean UnityMeshSimplifier.LODGeneratorHelper::get_IsGenerated()
extern void LODGeneratorHelper_get_IsGenerated_mDD401919EE858EC5E2DFCBAEB80C8A5946D20BF6 (void);
// 0x00000013 System.Void UnityMeshSimplifier.LODGeneratorHelper::Reset()
extern void LODGeneratorHelper_Reset_mAC6708F221035ADB74FD12E4CA5D2160C21AFB22 (void);
// 0x00000014 System.Void UnityMeshSimplifier.LODGeneratorHelper::.ctor()
extern void LODGeneratorHelper__ctor_m9C34426DF31BCF6D182690FA612443A2C2B01233 (void);
// 0x00000015 UnityEngine.LODGroup UnityMeshSimplifier.LODGenerator::GenerateLODs(UnityMeshSimplifier.LODGeneratorHelper)
extern void LODGenerator_GenerateLODs_mEC45CCA86CCC2795F61B2C4045296BB742B105C6 (void);
// 0x00000016 UnityEngine.LODGroup UnityMeshSimplifier.LODGenerator::GenerateLODs(UnityEngine.GameObject,UnityMeshSimplifier.LODLevel[],System.Boolean,UnityMeshSimplifier.SimplificationOptions)
extern void LODGenerator_GenerateLODs_mAD073DE852E5C015AC98CD5F808679453CD5C0E5 (void);
// 0x00000017 UnityEngine.LODGroup UnityMeshSimplifier.LODGenerator::GenerateLODs(UnityEngine.GameObject,UnityMeshSimplifier.LODLevel[],System.Boolean,UnityMeshSimplifier.SimplificationOptions,System.String)
extern void LODGenerator_GenerateLODs_m4C902CD913FE564F9BF01AD36EA8EBAAF495630B (void);
// 0x00000018 System.Boolean UnityMeshSimplifier.LODGenerator::DestroyLODs(UnityMeshSimplifier.LODGeneratorHelper)
extern void LODGenerator_DestroyLODs_m8BDFE19317DC351A0E67176300368D5271E7EADA (void);
// 0x00000019 System.Boolean UnityMeshSimplifier.LODGenerator::DestroyLODs(UnityEngine.GameObject)
extern void LODGenerator_DestroyLODs_mD594B741B58DCE8FC4E9752A5585F36DD38D4586 (void);
// 0x0000001A UnityMeshSimplifier.LODGenerator/StaticRenderer[] UnityMeshSimplifier.LODGenerator::GetStaticRenderers(UnityEngine.MeshRenderer[])
extern void LODGenerator_GetStaticRenderers_mF2A7EC0F35300A539F29149CBFB0D24CA0FC3E6D (void);
// 0x0000001B UnityMeshSimplifier.LODGenerator/SkinnedRenderer[] UnityMeshSimplifier.LODGenerator::GetSkinnedRenderers(UnityEngine.SkinnedMeshRenderer[])
extern void LODGenerator_GetSkinnedRenderers_m51BE3A0AF175EE7BD675F892C4AD8CD97C16E610 (void);
// 0x0000001C UnityMeshSimplifier.LODGenerator/StaticRenderer[] UnityMeshSimplifier.LODGenerator::CombineStaticMeshes(UnityEngine.Transform,System.Int32,UnityEngine.MeshRenderer[])
extern void LODGenerator_CombineStaticMeshes_mBCBFFB5CBA219C5A5D80DA211A84472CCBFD5A42 (void);
// 0x0000001D UnityMeshSimplifier.LODGenerator/SkinnedRenderer[] UnityMeshSimplifier.LODGenerator::CombineSkinnedMeshes(UnityEngine.Transform,System.Int32,UnityEngine.SkinnedMeshRenderer[])
extern void LODGenerator_CombineSkinnedMeshes_mE9AA423218EAA67A5A652BA9D8964C853A279A51 (void);
// 0x0000001E System.Void UnityMeshSimplifier.LODGenerator::ParentAndResetTransform(UnityEngine.Transform,UnityEngine.Transform)
extern void LODGenerator_ParentAndResetTransform_mCB3D6A8A23BECDF411DC44D2B13A482A57D71529 (void);
// 0x0000001F System.Void UnityMeshSimplifier.LODGenerator::ParentAndOffsetTransform(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform)
extern void LODGenerator_ParentAndOffsetTransform_m237D9A2A40FF936598F15B614FF467010B8558A7 (void);
// 0x00000020 UnityEngine.MeshRenderer UnityMeshSimplifier.LODGenerator::CreateLevelRenderer(System.String,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Mesh,UnityEngine.Material[],UnityMeshSimplifier.LODLevel&)
extern void LODGenerator_CreateLevelRenderer_mDC383DC8038E953AE5CD45E8CA8D069976E4B676 (void);
// 0x00000021 UnityEngine.SkinnedMeshRenderer UnityMeshSimplifier.LODGenerator::CreateSkinnedLevelRenderer(System.String,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Mesh,UnityEngine.Material[],UnityEngine.Transform,UnityEngine.Transform[],UnityMeshSimplifier.LODLevel&)
extern void LODGenerator_CreateSkinnedLevelRenderer_mD8C80E0F20DB7269315DE8C9E9377398D9D6739E (void);
// 0x00000022 UnityEngine.Transform UnityMeshSimplifier.LODGenerator::FindBestRootBone(UnityEngine.Transform,UnityEngine.SkinnedMeshRenderer[])
extern void LODGenerator_FindBestRootBone_mFA39FCC06C589BCFEA1D9BE7460863F173714372 (void);
// 0x00000023 System.Void UnityMeshSimplifier.LODGenerator::SetupLevelRenderer(UnityEngine.Renderer,UnityMeshSimplifier.LODLevel&)
extern void LODGenerator_SetupLevelRenderer_mC31214ECC3CAC955070530DCDFD256574A18B485 (void);
// 0x00000024 UnityEngine.Renderer[] UnityMeshSimplifier.LODGenerator::GetChildRenderersForLOD(UnityEngine.GameObject)
extern void LODGenerator_GetChildRenderersForLOD_m62708B6553FBD8B08C7F4E68ACB09FCEE020589F (void);
// 0x00000025 System.Void UnityMeshSimplifier.LODGenerator::CollectChildRenderersForLOD(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Renderer>)
extern void LODGenerator_CollectChildRenderersForLOD_m00122AE844566D484060F3B8A0CD7F193FA74853 (void);
// 0x00000026 UnityEngine.Mesh UnityMeshSimplifier.LODGenerator::SimplifyMesh(UnityEngine.Mesh,System.Single,UnityMeshSimplifier.SimplificationOptions)
extern void LODGenerator_SimplifyMesh_m31626950F41CF3DE5E54C8A57B8556DF0FE43F71 (void);
// 0x00000027 System.Void UnityMeshSimplifier.LODGenerator::DestroyObject(UnityEngine.Object)
extern void LODGenerator_DestroyObject_m584BC0C074AF1998706F8AD1A90579D0FD6FC48B (void);
// 0x00000028 System.Void UnityMeshSimplifier.LODGenerator::CreateBackup(UnityEngine.GameObject,UnityEngine.Renderer[])
extern void LODGenerator_CreateBackup_m66A6B9FA7892EEAA2546C96C2C50D9B273EB617B (void);
// 0x00000029 System.Void UnityMeshSimplifier.LODGenerator::RestoreBackup(UnityEngine.GameObject)
extern void LODGenerator_RestoreBackup_m8BD1BD599E7B50B0745CE9E17BD6028DD824B602 (void);
// 0x0000002A System.Void UnityMeshSimplifier.LODGenerator::DestroyLODAssets(UnityEngine.Transform)
extern void LODGenerator_DestroyLODAssets_m4810581B075BD5BFDFD59F40B2CED8D75AEFDEE1 (void);
// 0x0000002B System.Void UnityMeshSimplifier.LODGenerator::DestroyLODMaterialAsset(UnityEngine.Material)
extern void LODGenerator_DestroyLODMaterialAsset_mBA74ADB7F39982D3D5ECD9745BB49A1DCE870DA4 (void);
// 0x0000002C System.Void UnityMeshSimplifier.LODGenerator::DestroyLODAsset(UnityEngine.Object)
extern void LODGenerator_DestroyLODAsset_m3A75EA93CF14C798DE2FD82DC14B6E1113F93DE4 (void);
// 0x0000002D System.Void UnityMeshSimplifier.LODGenerator::SaveLODMeshAsset(UnityEngine.Object,System.String,System.String,System.Int32,System.String,System.String)
extern void LODGenerator_SaveLODMeshAsset_m005622DFE28FB6B7FCA0257CC28775EDAD7FB31D (void);
// 0x0000002E System.Void UnityMeshSimplifier.LODGenerator::SaveAsset(UnityEngine.Object,System.String)
extern void LODGenerator_SaveAsset_m2CDD4DE21C8F6AEE700A9819F3879385532251E5 (void);
// 0x0000002F System.Void UnityMeshSimplifier.LODGenerator::CreateParentDirectory(System.String)
extern void LODGenerator_CreateParentDirectory_m67D5A36916996F1281DC823842114EBDDDF74B19 (void);
// 0x00000030 System.String UnityMeshSimplifier.LODGenerator::MakePathSafe(System.String)
extern void LODGenerator_MakePathSafe_m7242E4619E3450DA70784FB7EDF59F72429AC92A (void);
// 0x00000031 System.String UnityMeshSimplifier.LODGenerator::ValidateSaveAssetsPath(System.String)
extern void LODGenerator_ValidateSaveAssetsPath_m866B84040957B9BD075EABB64F81A32F0FAABF14 (void);
// 0x00000032 System.Boolean UnityMeshSimplifier.LODGenerator::DeleteEmptyDirectory(System.String)
extern void LODGenerator_DeleteEmptyDirectory_m88DBEACAB0D9165E6F111F7AF6965AAD7B9A5ADF (void);
// 0x00000033 System.Void UnityMeshSimplifier.LODGenerator/<>c::.cctor()
extern void U3CU3Ec__cctor_mC7C00B3B0DF1073E1ACDEB2C1D82BB6AB88CD43C (void);
// 0x00000034 System.Void UnityMeshSimplifier.LODGenerator/<>c::.ctor()
extern void U3CU3Ec__ctor_m11FBF4D3DB42A18211CAFB729EC6B64DF96FF47E (void);
// 0x00000035 System.Boolean UnityMeshSimplifier.LODGenerator/<>c::<GenerateLODs>b__6_0(UnityEngine.Renderer)
extern void U3CU3Ec_U3CGenerateLODsU3Eb__6_0_m9B7F0722CF220551BC408F9C6478842C62979A50 (void);
// 0x00000036 UnityEngine.MeshRenderer UnityMeshSimplifier.LODGenerator/<>c::<GenerateLODs>b__6_1(UnityEngine.Renderer)
extern void U3CU3Ec_U3CGenerateLODsU3Eb__6_1_m92C7DB5AF7937FCDEE32E73D5FFEFFB3C2F3B7ED (void);
// 0x00000037 System.Boolean UnityMeshSimplifier.LODGenerator/<>c::<GenerateLODs>b__6_2(UnityEngine.Renderer)
extern void U3CU3Ec_U3CGenerateLODsU3Eb__6_2_m7C0B8CE0A97AD9896082B83E2390F96AB5530775 (void);
// 0x00000038 UnityEngine.SkinnedMeshRenderer UnityMeshSimplifier.LODGenerator/<>c::<GenerateLODs>b__6_3(UnityEngine.Renderer)
extern void U3CU3Ec_U3CGenerateLODsU3Eb__6_3_m6AB8926A919A160FF1451213A8648CC73ACCE93E (void);
// 0x00000039 System.Boolean UnityMeshSimplifier.LODGenerator/<>c::<CombineSkinnedMeshes>b__12_0(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_0_mE03C73D720099B35FBFA9C655F348D13B0F3A179 (void);
// 0x0000003A System.Boolean UnityMeshSimplifier.LODGenerator/<>c::<CombineSkinnedMeshes>b__12_1(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_1_mE5A077B9CE40DD645EC50D52A333AE3B768C6488 (void);
// 0x0000003B System.Boolean UnityMeshSimplifier.LODGenerator/<>c::<CombineSkinnedMeshes>b__12_2(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_2_mD58B8C1DFF878A6DD852010BD45A7A911A41837F (void);
// 0x0000003C System.Single UnityMeshSimplifier.LODLevel::get_ScreenRelativeTransitionHeight()
extern void LODLevel_get_ScreenRelativeTransitionHeight_m5F0B45ABD745D1698A23AEA0F02BAFFDC1F29E65 (void);
// 0x0000003D System.Void UnityMeshSimplifier.LODLevel::set_ScreenRelativeTransitionHeight(System.Single)
extern void LODLevel_set_ScreenRelativeTransitionHeight_mD1F80EEB33AA7CDA6E3DDCFE0EF2534D490AAE63 (void);
// 0x0000003E System.Single UnityMeshSimplifier.LODLevel::get_FadeTransitionWidth()
extern void LODLevel_get_FadeTransitionWidth_mEC23EF5E34EF6356784C1A123D916829A2B906DC (void);
// 0x0000003F System.Void UnityMeshSimplifier.LODLevel::set_FadeTransitionWidth(System.Single)
extern void LODLevel_set_FadeTransitionWidth_m739CA9CD6DB5D06F9FE785552FCE9A3006FF2CE7 (void);
// 0x00000040 System.Single UnityMeshSimplifier.LODLevel::get_Quality()
extern void LODLevel_get_Quality_m7DD83EEC2584A96168E12B5E6CB536F218898FF2 (void);
// 0x00000041 System.Void UnityMeshSimplifier.LODLevel::set_Quality(System.Single)
extern void LODLevel_set_Quality_m207E02075FE35B9A43BB864846C82D6A8743373C (void);
// 0x00000042 System.Boolean UnityMeshSimplifier.LODLevel::get_CombineMeshes()
extern void LODLevel_get_CombineMeshes_mB94F8EAF266789D367E056D4F49763A14679892A (void);
// 0x00000043 System.Void UnityMeshSimplifier.LODLevel::set_CombineMeshes(System.Boolean)
extern void LODLevel_set_CombineMeshes_m1F7F5638DEF0BB12D1AD22172150A27D57333809 (void);
// 0x00000044 System.Boolean UnityMeshSimplifier.LODLevel::get_CombineSubMeshes()
extern void LODLevel_get_CombineSubMeshes_mDDB737F80A03EBFA25576E2D0CC72536E91E8FAD (void);
// 0x00000045 System.Void UnityMeshSimplifier.LODLevel::set_CombineSubMeshes(System.Boolean)
extern void LODLevel_set_CombineSubMeshes_m2D31AC48566929B1E56C70932EDE4A8F7EA9BF0B (void);
// 0x00000046 UnityEngine.Renderer[] UnityMeshSimplifier.LODLevel::get_Renderers()
extern void LODLevel_get_Renderers_m86529300B7C1872F627E70B429AEB5C51BC35735 (void);
// 0x00000047 System.Void UnityMeshSimplifier.LODLevel::set_Renderers(UnityEngine.Renderer[])
extern void LODLevel_set_Renderers_mF2FCF33531538CBD0B1FFA1EFD553E5BCE96B6C1 (void);
// 0x00000048 UnityEngine.SkinQuality UnityMeshSimplifier.LODLevel::get_SkinQuality()
extern void LODLevel_get_SkinQuality_mF8A2297264057180920DEEFD3EE46EDDCDA3213E (void);
// 0x00000049 System.Void UnityMeshSimplifier.LODLevel::set_SkinQuality(UnityEngine.SkinQuality)
extern void LODLevel_set_SkinQuality_m1794B41EFBA468CEECC262884AF7EABB02232567 (void);
// 0x0000004A UnityEngine.Rendering.ShadowCastingMode UnityMeshSimplifier.LODLevel::get_ShadowCastingMode()
extern void LODLevel_get_ShadowCastingMode_mA0DDA97611108A166E805C73E202D97E9FEF225E (void);
// 0x0000004B System.Void UnityMeshSimplifier.LODLevel::set_ShadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern void LODLevel_set_ShadowCastingMode_m38CFB075167A200EFE36221A2BD6591C842CDA1A (void);
// 0x0000004C System.Boolean UnityMeshSimplifier.LODLevel::get_ReceiveShadows()
extern void LODLevel_get_ReceiveShadows_m1438F60100335FDDB49A4A57347DB44294C6C184 (void);
// 0x0000004D System.Void UnityMeshSimplifier.LODLevel::set_ReceiveShadows(System.Boolean)
extern void LODLevel_set_ReceiveShadows_m8CF1D403BA211C5C92E40CA83FFFE68A4179ED10 (void);
// 0x0000004E UnityEngine.MotionVectorGenerationMode UnityMeshSimplifier.LODLevel::get_MotionVectorGenerationMode()
extern void LODLevel_get_MotionVectorGenerationMode_m15C482F7280CDA7F02B4A6494381EE37EE6CC941 (void);
// 0x0000004F System.Void UnityMeshSimplifier.LODLevel::set_MotionVectorGenerationMode(UnityEngine.MotionVectorGenerationMode)
extern void LODLevel_set_MotionVectorGenerationMode_mDB12EA048CFD6BE27E6B014664B0028EB73C52EB (void);
// 0x00000050 System.Boolean UnityMeshSimplifier.LODLevel::get_SkinnedMotionVectors()
extern void LODLevel_get_SkinnedMotionVectors_mAFAC1AF1AAF57F87C89C545559815901F99620C9 (void);
// 0x00000051 System.Void UnityMeshSimplifier.LODLevel::set_SkinnedMotionVectors(System.Boolean)
extern void LODLevel_set_SkinnedMotionVectors_m859FDD4765F3D970A417B9D754EB9754070FAA95 (void);
// 0x00000052 UnityEngine.Rendering.LightProbeUsage UnityMeshSimplifier.LODLevel::get_LightProbeUsage()
extern void LODLevel_get_LightProbeUsage_m53514686041B8FF574395D312CA9C50B177C7673 (void);
// 0x00000053 System.Void UnityMeshSimplifier.LODLevel::set_LightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
extern void LODLevel_set_LightProbeUsage_m5265F48D485C6E25CA8502DBBC28859E22E50DB4 (void);
// 0x00000054 UnityEngine.Rendering.ReflectionProbeUsage UnityMeshSimplifier.LODLevel::get_ReflectionProbeUsage()
extern void LODLevel_get_ReflectionProbeUsage_mCCD08D7BA5CF741FE718AD98002AC10730D8AF80 (void);
// 0x00000055 System.Void UnityMeshSimplifier.LODLevel::set_ReflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
extern void LODLevel_set_ReflectionProbeUsage_mE07C5D8A69773598EE6D7138DADB99A73FDE24B1 (void);
// 0x00000056 System.Void UnityMeshSimplifier.LODLevel::.ctor(System.Single,System.Single)
extern void LODLevel__ctor_m83AF36B169F6391CCA9C9525A10E8112D5BBC83B (void);
// 0x00000057 System.Void UnityMeshSimplifier.LODLevel::.ctor(System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern void LODLevel__ctor_m1BCE73707FC8C4E53041818FC6EA9EA1B9FA93EC (void);
// 0x00000058 System.Void UnityMeshSimplifier.LODLevel::.ctor(System.Single,System.Single,System.Single,System.Boolean,System.Boolean,UnityEngine.Renderer[])
extern void LODLevel__ctor_m2A7EBCEF83EC7D7E89DBC8137D60BD0A562B29DE (void);
// 0x00000059 UnityEngine.Mesh UnityMeshSimplifier.MeshCombiner::CombineMeshes(UnityEngine.Transform,UnityEngine.MeshRenderer[],UnityEngine.Material[]&)
extern void MeshCombiner_CombineMeshes_mCF6219DA18AA1F32F9A0EB469D3152FB77CAAA30 (void);
// 0x0000005A UnityEngine.Mesh UnityMeshSimplifier.MeshCombiner::CombineMeshes(UnityEngine.Transform,UnityEngine.SkinnedMeshRenderer[],UnityEngine.Material[]&,UnityEngine.Transform[]&)
extern void MeshCombiner_CombineMeshes_mB270F6C283B18CDB5ACA1A26E431F47801F931D5 (void);
// 0x0000005B UnityEngine.Mesh UnityMeshSimplifier.MeshCombiner::CombineMeshes(UnityEngine.Mesh[],UnityEngine.Matrix4x4[],UnityEngine.Material[][],UnityEngine.Material[]&)
extern void MeshCombiner_CombineMeshes_mB87158AC41F3DE9EE85D114D8B2F10B40CD232EC (void);
// 0x0000005C UnityEngine.Mesh UnityMeshSimplifier.MeshCombiner::CombineMeshes(UnityEngine.Mesh[],UnityEngine.Matrix4x4[],UnityEngine.Material[][],UnityEngine.Transform[][],UnityEngine.Material[]&,UnityEngine.Transform[]&)
extern void MeshCombiner_CombineMeshes_m1179F48E91FA3016AF01779C0D85B285E331D73B (void);
// 0x0000005D System.Void UnityMeshSimplifier.MeshCombiner::CopyVertexPositions(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3[])
extern void MeshCombiner_CopyVertexPositions_mBAC06F7623C2CE550A8B33F02F4343E16223C820 (void);
// 0x0000005E System.Void UnityMeshSimplifier.MeshCombiner::CopyVertexAttributes(System.Collections.Generic.List`1<T>&,System.Collections.Generic.IEnumerable`1<T>,System.Int32,System.Int32,System.Int32,T)
// 0x0000005F T[] UnityMeshSimplifier.MeshCombiner::MergeArrays(T[],T[])
// 0x00000060 System.Void UnityMeshSimplifier.MeshCombiner::TransformVertices(UnityEngine.Vector3[],UnityEngine.Matrix4x4&)
extern void MeshCombiner_TransformVertices_m3D2F3E48FB093E5F1EB538DEA73031B04A33FB71 (void);
// 0x00000061 System.Void UnityMeshSimplifier.MeshCombiner::TransformNormals(UnityEngine.Vector3[],UnityEngine.Matrix4x4&)
extern void MeshCombiner_TransformNormals_m0436EB2E10CDF4434882A29F24E3CA4A5DED2C43 (void);
// 0x00000062 System.Void UnityMeshSimplifier.MeshCombiner::TransformTangents(UnityEngine.Vector4[],UnityEngine.Matrix4x4&)
extern void MeshCombiner_TransformTangents_m48D2A4696DCB89AD8E2127711C9E142B0F1763D4 (void);
// 0x00000063 System.Void UnityMeshSimplifier.MeshCombiner::TransformVertices(UnityEngine.Vector3[],UnityEngine.BoneWeight[],UnityEngine.Matrix4x4[],UnityEngine.Matrix4x4[])
extern void MeshCombiner_TransformVertices_m21B26E4BE1D7EC98330A642BD3CD72DBC4F529A3 (void);
// 0x00000064 System.Void UnityMeshSimplifier.MeshCombiner::RemapBones(UnityEngine.BoneWeight[],System.Int32[])
extern void MeshCombiner_RemapBones_mE78155E50D07CAC92F7A6AD2D0429A532D7F8F7E (void);
// 0x00000065 UnityEngine.Matrix4x4 UnityMeshSimplifier.MeshCombiner::ScaleMatrix(UnityEngine.Matrix4x4&,System.Single)
extern void MeshCombiner_ScaleMatrix_mF7BD1663F7267C3EEC81B7B8626627889441858D (void);
// 0x00000066 System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveBorders()
extern void MeshSimplifier_get_PreserveBorders_m8C4FC4061BF1CC6B7321EE01E12734754340B9AF (void);
// 0x00000067 System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveBorders(System.Boolean)
extern void MeshSimplifier_set_PreserveBorders_mC1C47FA150D6D2F75AF50F24C814D01E746705CD (void);
// 0x00000068 System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveBorderEdges()
extern void MeshSimplifier_get_PreserveBorderEdges_mCC3DA5AB8FF239A1F66A41A90F861AE95A0D41BA (void);
// 0x00000069 System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveBorderEdges(System.Boolean)
extern void MeshSimplifier_set_PreserveBorderEdges_m44EADEC6EE08A2271AF515DBA2052450D6A867B1 (void);
// 0x0000006A System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveSeams()
extern void MeshSimplifier_get_PreserveSeams_m39802B9C123D2ABCFA43ADE126AA4EB3B19C74A7 (void);
// 0x0000006B System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveSeams(System.Boolean)
extern void MeshSimplifier_set_PreserveSeams_m8175AE9BA05811AC1306144641DED96DE87FCC51 (void);
// 0x0000006C System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveUVSeamEdges()
extern void MeshSimplifier_get_PreserveUVSeamEdges_m1DB17F3C09F3AC704651F0C8A1400EF95C05E302 (void);
// 0x0000006D System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveUVSeamEdges(System.Boolean)
extern void MeshSimplifier_set_PreserveUVSeamEdges_mA7C05E9E09256B050D672005F3081B6EA1C479EE (void);
// 0x0000006E System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveFoldovers()
extern void MeshSimplifier_get_PreserveFoldovers_m098FAF6D5AB415924618746A24F3E58E68A8F5A6 (void);
// 0x0000006F System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveFoldovers(System.Boolean)
extern void MeshSimplifier_set_PreserveFoldovers_mFFE2927DFA81B8F746794060CD7B2AAA21CA4CD3 (void);
// 0x00000070 System.Boolean UnityMeshSimplifier.MeshSimplifier::get_PreserveUVFoldoverEdges()
extern void MeshSimplifier_get_PreserveUVFoldoverEdges_m3BB059A4BC1F429A70446E3430061C3D85E8B477 (void);
// 0x00000071 System.Void UnityMeshSimplifier.MeshSimplifier::set_PreserveUVFoldoverEdges(System.Boolean)
extern void MeshSimplifier_set_PreserveUVFoldoverEdges_mA56BD8D3CC60D2CB6C92D4A939F783EF0613C816 (void);
// 0x00000072 System.Boolean UnityMeshSimplifier.MeshSimplifier::get_EnableSmartLink()
extern void MeshSimplifier_get_EnableSmartLink_m0C0C218DE13678D7CAE30567B8111C8DD6310B49 (void);
// 0x00000073 System.Void UnityMeshSimplifier.MeshSimplifier::set_EnableSmartLink(System.Boolean)
extern void MeshSimplifier_set_EnableSmartLink_mCEE7B775817B8578BFD0D65DFAA508B4602F174B (void);
// 0x00000074 System.Int32 UnityMeshSimplifier.MeshSimplifier::get_MaxIterationCount()
extern void MeshSimplifier_get_MaxIterationCount_mF44B7EB8E333AD67E7BD21179CBCE8EB24B6B587 (void);
// 0x00000075 System.Void UnityMeshSimplifier.MeshSimplifier::set_MaxIterationCount(System.Int32)
extern void MeshSimplifier_set_MaxIterationCount_m3D4E7C2463B080BB228F88380B9AFA7E35DCD649 (void);
// 0x00000076 System.Double UnityMeshSimplifier.MeshSimplifier::get_Agressiveness()
extern void MeshSimplifier_get_Agressiveness_mEA3B0110264F8487F1F4FDBEE6B30D91A4A9BD1E (void);
// 0x00000077 System.Void UnityMeshSimplifier.MeshSimplifier::set_Agressiveness(System.Double)
extern void MeshSimplifier_set_Agressiveness_m0C9D2ED4021C08A690B58BFCA2011AD28C8B0CB6 (void);
// 0x00000078 System.Boolean UnityMeshSimplifier.MeshSimplifier::get_Verbose()
extern void MeshSimplifier_get_Verbose_mC2967C01E671DDD4E7F416D35736BA0659BAC6F2 (void);
// 0x00000079 System.Void UnityMeshSimplifier.MeshSimplifier::set_Verbose(System.Boolean)
extern void MeshSimplifier_set_Verbose_m410F378A9CB49414184712D9ABCDD742FA05318C (void);
// 0x0000007A System.Double UnityMeshSimplifier.MeshSimplifier::get_VertexLinkDistance()
extern void MeshSimplifier_get_VertexLinkDistance_m72BFAD0F16B32F07A04F41A3837BBC438DAFE8A8 (void);
// 0x0000007B System.Void UnityMeshSimplifier.MeshSimplifier::set_VertexLinkDistance(System.Double)
extern void MeshSimplifier_set_VertexLinkDistance_m68DEC17A7CC7C436120C717E1A4BB3D836FE9708 (void);
// 0x0000007C System.Double UnityMeshSimplifier.MeshSimplifier::get_VertexLinkDistanceSqr()
extern void MeshSimplifier_get_VertexLinkDistanceSqr_m695BC4BA12E89E9B4445E0F2A8DC73EBCD4C5EE0 (void);
// 0x0000007D System.Void UnityMeshSimplifier.MeshSimplifier::set_VertexLinkDistanceSqr(System.Double)
extern void MeshSimplifier_set_VertexLinkDistanceSqr_m8D7B10B31C909F26D0997DEE78F9434EC77840A7 (void);
// 0x0000007E UnityEngine.Vector3[] UnityMeshSimplifier.MeshSimplifier::get_Vertices()
extern void MeshSimplifier_get_Vertices_mF63825865EA820837705885F1FA400995A7E33CE (void);
// 0x0000007F System.Void UnityMeshSimplifier.MeshSimplifier::set_Vertices(UnityEngine.Vector3[])
extern void MeshSimplifier_set_Vertices_mEA49F68B76E315B8C8222355DAD611E1A81A6B08 (void);
// 0x00000080 System.Int32 UnityMeshSimplifier.MeshSimplifier::get_SubMeshCount()
extern void MeshSimplifier_get_SubMeshCount_m49DEBC4DB9CB5DF13A626781004432F81F05FB8C (void);
// 0x00000081 System.Int32 UnityMeshSimplifier.MeshSimplifier::get_BlendShapeCount()
extern void MeshSimplifier_get_BlendShapeCount_m2385E18AF64C1C1F6ED43F2E8581003601026FBF (void);
// 0x00000082 UnityEngine.Vector3[] UnityMeshSimplifier.MeshSimplifier::get_Normals()
extern void MeshSimplifier_get_Normals_m4010FF45261167E95780FD92E89A566A23D0D28B (void);
// 0x00000083 System.Void UnityMeshSimplifier.MeshSimplifier::set_Normals(UnityEngine.Vector3[])
extern void MeshSimplifier_set_Normals_m5D544D2B7EE28E5428ED42FC25243EC542BC55FD (void);
// 0x00000084 UnityEngine.Vector4[] UnityMeshSimplifier.MeshSimplifier::get_Tangents()
extern void MeshSimplifier_get_Tangents_m42EB38FEBBBBBAE567D1766252D336482E975FF5 (void);
// 0x00000085 System.Void UnityMeshSimplifier.MeshSimplifier::set_Tangents(UnityEngine.Vector4[])
extern void MeshSimplifier_set_Tangents_mD016AC9103F8C94AB96F8CE0F4C6ED99785C9E4D (void);
// 0x00000086 UnityEngine.Vector2[] UnityMeshSimplifier.MeshSimplifier::get_UV1()
extern void MeshSimplifier_get_UV1_mC77A342C1E14A2786FA5E47D9A27FFB5C76545A7 (void);
// 0x00000087 System.Void UnityMeshSimplifier.MeshSimplifier::set_UV1(UnityEngine.Vector2[])
extern void MeshSimplifier_set_UV1_m908AFF8A57B0646769139BCFB82C55FB15772459 (void);
// 0x00000088 UnityEngine.Vector2[] UnityMeshSimplifier.MeshSimplifier::get_UV2()
extern void MeshSimplifier_get_UV2_m4516F296BE3AEB92DD7FA15EA6E819B75157A867 (void);
// 0x00000089 System.Void UnityMeshSimplifier.MeshSimplifier::set_UV2(UnityEngine.Vector2[])
extern void MeshSimplifier_set_UV2_mF6F525E5331826A10C6590D34387F99F5A623084 (void);
// 0x0000008A UnityEngine.Vector2[] UnityMeshSimplifier.MeshSimplifier::get_UV3()
extern void MeshSimplifier_get_UV3_mA200738AF386B7F674DF9BDDA3DE5E3FA68E5811 (void);
// 0x0000008B System.Void UnityMeshSimplifier.MeshSimplifier::set_UV3(UnityEngine.Vector2[])
extern void MeshSimplifier_set_UV3_m39214DD70A0EBB1222832CEA35E7F2F250BCC2EF (void);
// 0x0000008C UnityEngine.Vector2[] UnityMeshSimplifier.MeshSimplifier::get_UV4()
extern void MeshSimplifier_get_UV4_mE3A040D906F56BE80966BCE46B47C83BA5A6914D (void);
// 0x0000008D System.Void UnityMeshSimplifier.MeshSimplifier::set_UV4(UnityEngine.Vector2[])
extern void MeshSimplifier_set_UV4_m33C527408B06B96E9DE7D29C3B0191E34CB9E67E (void);
// 0x0000008E UnityEngine.Color[] UnityMeshSimplifier.MeshSimplifier::get_Colors()
extern void MeshSimplifier_get_Colors_mAC7B2ADD361E3C8E2C9195FB532A71B271F6A4D1 (void);
// 0x0000008F System.Void UnityMeshSimplifier.MeshSimplifier::set_Colors(UnityEngine.Color[])
extern void MeshSimplifier_set_Colors_mD520933E0B454F5B3A959941574A365867C2EE29 (void);
// 0x00000090 UnityEngine.BoneWeight[] UnityMeshSimplifier.MeshSimplifier::get_BoneWeights()
extern void MeshSimplifier_get_BoneWeights_m7CB0CB54F0BFFE9094B38D7594EE14815F342CF6 (void);
// 0x00000091 System.Void UnityMeshSimplifier.MeshSimplifier::set_BoneWeights(UnityEngine.BoneWeight[])
extern void MeshSimplifier_set_BoneWeights_m6269DBDAF129ABC14DA57D6EEA25E3020963F363 (void);
// 0x00000092 System.Void UnityMeshSimplifier.MeshSimplifier::.ctor()
extern void MeshSimplifier__ctor_m8F671CF05AD7B4AB2299F93EE42BD8BCC4304FF6 (void);
// 0x00000093 System.Void UnityMeshSimplifier.MeshSimplifier::.ctor(UnityEngine.Mesh)
extern void MeshSimplifier__ctor_mDF5550C531DFA38AA35A82E26453BFE455D59039 (void);
// 0x00000094 System.Void UnityMeshSimplifier.MeshSimplifier::InitializeVertexAttribute(T[],UnityMeshSimplifier.ResizableArray`1<T>&,System.String)
// 0x00000095 System.Double UnityMeshSimplifier.MeshSimplifier::VertexError(UnityMeshSimplifier.SymmetricMatrix&,System.Double,System.Double,System.Double)
extern void MeshSimplifier_VertexError_m044186C81022374C27E83978B9E2B06F04914BA2 (void);
// 0x00000096 System.Double UnityMeshSimplifier.MeshSimplifier::CalculateError(UnityMeshSimplifier.MeshSimplifier/Vertex&,UnityMeshSimplifier.MeshSimplifier/Vertex&,UnityMeshSimplifier.Vector3d&)
extern void MeshSimplifier_CalculateError_m1BBBED9A2BABAABC00DFCBE9FD3C8956C1D3358B (void);
// 0x00000097 System.Void UnityMeshSimplifier.MeshSimplifier::CalculateBarycentricCoords(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityEngine.Vector3&)
extern void MeshSimplifier_CalculateBarycentricCoords_m457252058905579CA49FFA5953480AC5F84A02D5 (void);
// 0x00000098 UnityEngine.Vector4 UnityMeshSimplifier.MeshSimplifier::NormalizeTangent(UnityEngine.Vector4)
extern void MeshSimplifier_NormalizeTangent_m09069F776466F1C3F53C340474D83AF2DC4F04CF (void);
// 0x00000099 System.Boolean UnityMeshSimplifier.MeshSimplifier::Flipped(UnityMeshSimplifier.Vector3d&,System.Int32,System.Int32,UnityMeshSimplifier.MeshSimplifier/Vertex&,System.Boolean[])
extern void MeshSimplifier_Flipped_m4CE3ECB82844E1DCB0DEF0A4CD2EE75F1C87BF14 (void);
// 0x0000009A System.Void UnityMeshSimplifier.MeshSimplifier::UpdateTriangles(System.Int32,System.Int32,UnityMeshSimplifier.MeshSimplifier/Vertex&,UnityMeshSimplifier.ResizableArray`1<System.Boolean>,System.Int32&)
extern void MeshSimplifier_UpdateTriangles_mC8FF0F2720577392C7F3A167ABDFDDCFF271C39F (void);
// 0x0000009B System.Void UnityMeshSimplifier.MeshSimplifier::InterpolateVertexAttributes(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3&)
extern void MeshSimplifier_InterpolateVertexAttributes_mA64D9BB4B51F373A3A16F3FF6D132B73153A2F7D (void);
// 0x0000009C System.Boolean UnityMeshSimplifier.MeshSimplifier::AreUVsTheSame(System.Int32,System.Int32,System.Int32)
extern void MeshSimplifier_AreUVsTheSame_m45B80546BAE1FFC13B94057DC86310F57300EDF7 (void);
// 0x0000009D System.Void UnityMeshSimplifier.MeshSimplifier::RemoveVertexPass(System.Int32,System.Int32,System.Double,UnityMeshSimplifier.ResizableArray`1<System.Boolean>,UnityMeshSimplifier.ResizableArray`1<System.Boolean>,System.Int32&)
extern void MeshSimplifier_RemoveVertexPass_m3FD80A7FA1E9933B0FAEAA5FC9B34D63C07903BF (void);
// 0x0000009E System.Void UnityMeshSimplifier.MeshSimplifier::UpdateMesh(System.Int32)
extern void MeshSimplifier_UpdateMesh_mFC2D87867499C208C52D4BD8E9D4E224BC23E524 (void);
// 0x0000009F System.Void UnityMeshSimplifier.MeshSimplifier::UpdateReferences()
extern void MeshSimplifier_UpdateReferences_m9AD7EF93152C0F9DAB0AC62B2522BB14A67583F4 (void);
// 0x000000A0 System.Void UnityMeshSimplifier.MeshSimplifier::CompactMesh()
extern void MeshSimplifier_CompactMesh_m246B6008FA9727B9900BEFC60E1EE85A7C7EE213 (void);
// 0x000000A1 System.Void UnityMeshSimplifier.MeshSimplifier::CalculateSubMeshOffsets()
extern void MeshSimplifier_CalculateSubMeshOffsets_m2767488A342E2D188E6830C05A0215EBED85540F (void);
// 0x000000A2 System.Int32[][] UnityMeshSimplifier.MeshSimplifier::GetAllSubMeshTriangles()
extern void MeshSimplifier_GetAllSubMeshTriangles_m3865BB75CA531D49EA56831A979DDB5D3CD9B3FD (void);
// 0x000000A3 System.Int32[] UnityMeshSimplifier.MeshSimplifier::GetSubMeshTriangles(System.Int32)
extern void MeshSimplifier_GetSubMeshTriangles_mC796C504580391E60B9BB2A5E9545F25ECB4A92D (void);
// 0x000000A4 System.Void UnityMeshSimplifier.MeshSimplifier::ClearSubMeshes()
extern void MeshSimplifier_ClearSubMeshes_m3B0D1D3B080630E6304362EE01F8C5C138BAE015 (void);
// 0x000000A5 System.Void UnityMeshSimplifier.MeshSimplifier::AddSubMeshTriangles(System.Int32[])
extern void MeshSimplifier_AddSubMeshTriangles_m85C05C1AF2E7338F6ADC6138BD3259CAD0C0F441 (void);
// 0x000000A6 System.Void UnityMeshSimplifier.MeshSimplifier::AddSubMeshTriangles(System.Int32[][])
extern void MeshSimplifier_AddSubMeshTriangles_m5AF7025ABE70F303B48FA7727BC3A5BDCE23EA84 (void);
// 0x000000A7 UnityEngine.Vector2[] UnityMeshSimplifier.MeshSimplifier::GetUVs2D(System.Int32)
extern void MeshSimplifier_GetUVs2D_m569776C010F7857FDA45A1964E427BB8FD19D48F (void);
// 0x000000A8 UnityEngine.Vector3[] UnityMeshSimplifier.MeshSimplifier::GetUVs3D(System.Int32)
extern void MeshSimplifier_GetUVs3D_m75B142FCF183B4CA043D4FA4F18B6677E7E03C88 (void);
// 0x000000A9 UnityEngine.Vector4[] UnityMeshSimplifier.MeshSimplifier::GetUVs4D(System.Int32)
extern void MeshSimplifier_GetUVs4D_m98D9E49923F8D07B29A7F2015D1BC11B9EF9CBBC (void);
// 0x000000AA System.Void UnityMeshSimplifier.MeshSimplifier::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void MeshSimplifier_GetUVs_m5B3DDB579B25D0F0B2A88351CB9429B4CEAC0887 (void);
// 0x000000AB System.Void UnityMeshSimplifier.MeshSimplifier::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void MeshSimplifier_GetUVs_mF2AE33305BE6F36E69F2B880092FDA9BD5465791 (void);
// 0x000000AC System.Void UnityMeshSimplifier.MeshSimplifier::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshSimplifier_GetUVs_m8AF3296700235E9776FA19E068D18D3CD1CDCDE5 (void);
// 0x000000AD System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,UnityEngine.Vector2[])
extern void MeshSimplifier_SetUVs_mC535FEE97DC218268D036E45ED0F94B32732AD84 (void);
// 0x000000AE System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,UnityEngine.Vector3[])
extern void MeshSimplifier_SetUVs_m50D2209B26BDE9888E3631BDD72D0533C25AF2FD (void);
// 0x000000AF System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,UnityEngine.Vector4[])
extern void MeshSimplifier_SetUVs_mD1097423C57D12494C4D8DAAA3F3AAD6A4EBF2F1 (void);
// 0x000000B0 System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void MeshSimplifier_SetUVs_m7044D6D18F65BBC318A370A0A2AB3C4CE8C8ADC2 (void);
// 0x000000B1 System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void MeshSimplifier_SetUVs_m3215E2332B75F158E2324FD0A523DBE4CFA14E81 (void);
// 0x000000B2 System.Void UnityMeshSimplifier.MeshSimplifier::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshSimplifier_SetUVs_mEA7225D12789927D11EDCEBBCEFB362835C11CA5 (void);
// 0x000000B3 System.Void UnityMeshSimplifier.MeshSimplifier::SetUVsAuto(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshSimplifier_SetUVsAuto_m23DD2015AFB94EE6AD2BCBFDA256795EEB97DF0D (void);
// 0x000000B4 UnityMeshSimplifier.BlendShape[] UnityMeshSimplifier.MeshSimplifier::GetAllBlendShapes()
extern void MeshSimplifier_GetAllBlendShapes_mDA38116AA081E2B83ECE86F6D213EE7A0D6F9853 (void);
// 0x000000B5 UnityMeshSimplifier.BlendShape UnityMeshSimplifier.MeshSimplifier::GetBlendShape(System.Int32)
extern void MeshSimplifier_GetBlendShape_mAD37153429C8D1C7421279991644E582CEC653A4 (void);
// 0x000000B6 System.Void UnityMeshSimplifier.MeshSimplifier::ClearBlendShapes()
extern void MeshSimplifier_ClearBlendShapes_mA6A7D610FAC2286DDE65DD071399C0504193BE5F (void);
// 0x000000B7 System.Void UnityMeshSimplifier.MeshSimplifier::AddBlendShape(UnityMeshSimplifier.BlendShape)
extern void MeshSimplifier_AddBlendShape_m6F8DE7A349E01AF6D17378B2784CA0F14059C354 (void);
// 0x000000B8 System.Void UnityMeshSimplifier.MeshSimplifier::AddBlendShapes(UnityMeshSimplifier.BlendShape[])
extern void MeshSimplifier_AddBlendShapes_mC05996B1FF9F118CE995CE10851310A5CA52C6D4 (void);
// 0x000000B9 System.Void UnityMeshSimplifier.MeshSimplifier::Initialize(UnityEngine.Mesh)
extern void MeshSimplifier_Initialize_mEDB0A96361FE766A8A3C921A05DFE25FEFF7CD01 (void);
// 0x000000BA System.Void UnityMeshSimplifier.MeshSimplifier::SimplifyMesh(System.Single)
extern void MeshSimplifier_SimplifyMesh_mF645D1B4BB52C39374CA402085138E591D921A9A (void);
// 0x000000BB System.Void UnityMeshSimplifier.MeshSimplifier::SimplifyMeshLossless()
extern void MeshSimplifier_SimplifyMeshLossless_mCDD5FADACDC61B66F16297E65A9EE8759BCFC77D (void);
// 0x000000BC UnityEngine.Mesh UnityMeshSimplifier.MeshSimplifier::ToMesh()
extern void MeshSimplifier_ToMesh_m5FFB789E8E7D40BBC473564D73629383FED01E0D (void);
// 0x000000BD System.Int32 UnityMeshSimplifier.MeshSimplifier/Triangle::get_Item(System.Int32)
extern void Triangle_get_Item_m8340531208756C9FA4D5F390799CAD4B0505D71C (void);
// 0x000000BE System.Void UnityMeshSimplifier.MeshSimplifier/Triangle::set_Item(System.Int32,System.Int32)
extern void Triangle_set_Item_m2FE750EB184609848FF49F65457727F79D3039D0 (void);
// 0x000000BF System.Void UnityMeshSimplifier.MeshSimplifier/Triangle::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Triangle__ctor_mF9C562B7EE15038584BC7B7E2892CBDBB828CBBE (void);
// 0x000000C0 System.Void UnityMeshSimplifier.MeshSimplifier/Triangle::GetAttributeIndices(System.Int32[])
extern void Triangle_GetAttributeIndices_mB36F4A82121F20D5E38A9A1674D53B8485FB64CE (void);
// 0x000000C1 System.Void UnityMeshSimplifier.MeshSimplifier/Triangle::SetAttributeIndex(System.Int32,System.Int32)
extern void Triangle_SetAttributeIndex_m386E04F01CD553230913A921065F818AB178319D (void);
// 0x000000C2 System.Void UnityMeshSimplifier.MeshSimplifier/Triangle::GetErrors(System.Double[])
extern void Triangle_GetErrors_m9005DCF223C7FF48287C8695753C3DFCF861BB59 (void);
// 0x000000C3 System.Void UnityMeshSimplifier.MeshSimplifier/Vertex::.ctor(UnityMeshSimplifier.Vector3d)
extern void Vertex__ctor_mCD04A9DCA2249D64D5D3CA9E4484FDA2201712FB (void);
// 0x000000C4 System.Void UnityMeshSimplifier.MeshSimplifier/Ref::Set(System.Int32,System.Int32)
extern void Ref_Set_mE9B966F214EA0F9A1FA0A69FFBC22DC159C5743F (void);
// 0x000000C5 TVec[][] UnityMeshSimplifier.MeshSimplifier/UVChannels`1::get_Data()
// 0x000000C6 UnityMeshSimplifier.ResizableArray`1<TVec> UnityMeshSimplifier.MeshSimplifier/UVChannels`1::get_Item(System.Int32)
// 0x000000C7 System.Void UnityMeshSimplifier.MeshSimplifier/UVChannels`1::set_Item(System.Int32,UnityMeshSimplifier.ResizableArray`1<TVec>)
// 0x000000C8 System.Void UnityMeshSimplifier.MeshSimplifier/UVChannels`1::.ctor()
// 0x000000C9 System.Void UnityMeshSimplifier.MeshSimplifier/UVChannels`1::Resize(System.Int32,System.Boolean)
// 0x000000CA System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeContainer::.ctor(UnityMeshSimplifier.BlendShape)
extern void BlendShapeContainer__ctor_m76AEDC83F0880C099B85148C586B70A25F612F27 (void);
// 0x000000CB System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeContainer::MoveVertexElement(System.Int32,System.Int32)
extern void BlendShapeContainer_MoveVertexElement_m5D14043CDF543A06DF4BEEEAC11366FE8AE38AEC (void);
// 0x000000CC System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeContainer::InterpolateVertexAttributes(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3&)
extern void BlendShapeContainer_InterpolateVertexAttributes_mBC395CB54586B3FB92F292AF965B1F504DBB84CF (void);
// 0x000000CD System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeContainer::Resize(System.Int32,System.Boolean)
extern void BlendShapeContainer_Resize_m21A42F9C7C06D74CEE89BB048E15638F4A899B63 (void);
// 0x000000CE UnityMeshSimplifier.BlendShape UnityMeshSimplifier.MeshSimplifier/BlendShapeContainer::ToBlendShape()
extern void BlendShapeContainer_ToBlendShape_mB726B1F9B05EABAA9B68F0897FAC6B15CAE265A1 (void);
// 0x000000CF System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeFrameContainer::.ctor(UnityMeshSimplifier.BlendShapeFrame)
extern void BlendShapeFrameContainer__ctor_m9FA507F084341EF12A2DFD1F188A5AC6169361F5 (void);
// 0x000000D0 System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeFrameContainer::MoveVertexElement(System.Int32,System.Int32)
extern void BlendShapeFrameContainer_MoveVertexElement_m13A9EBD09F1154B6DACACF04A32C743B4C6020D4 (void);
// 0x000000D1 System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeFrameContainer::InterpolateVertexAttributes(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3&)
extern void BlendShapeFrameContainer_InterpolateVertexAttributes_m3594E774FCB789BE3DD3F086BFD16CAAD1812F32 (void);
// 0x000000D2 System.Void UnityMeshSimplifier.MeshSimplifier/BlendShapeFrameContainer::Resize(System.Int32,System.Boolean)
extern void BlendShapeFrameContainer_Resize_m2BD253A130FF46436BADCC2B266ACCB2CA2F81EE (void);
// 0x000000D3 UnityMeshSimplifier.BlendShapeFrame UnityMeshSimplifier.MeshSimplifier/BlendShapeFrameContainer::ToBlendShapeFrame()
extern void BlendShapeFrameContainer_ToBlendShapeFrame_mA2731B1B58C7C8ADA6416FBE6D1C4BF183608133 (void);
// 0x000000D4 System.Void UnityMeshSimplifier.MeshSimplifier/BorderVertex::.ctor(System.Int32,System.Int32)
extern void BorderVertex__ctor_mAB9D595A3D6329170C044F31E9D8167C7C57E179 (void);
// 0x000000D5 System.Int32 UnityMeshSimplifier.MeshSimplifier/BorderVertexComparer::Compare(UnityMeshSimplifier.MeshSimplifier/BorderVertex,UnityMeshSimplifier.MeshSimplifier/BorderVertex)
extern void BorderVertexComparer_Compare_mA6795FE7D80BC4620DBD618CBC9CF12410951AE5 (void);
// 0x000000D6 System.Void UnityMeshSimplifier.MeshSimplifier/BorderVertexComparer::.ctor()
extern void BorderVertexComparer__ctor_m650C577E446FA170F476DD597B960CDDD84A888C (void);
// 0x000000D7 System.Void UnityMeshSimplifier.MeshSimplifier/BorderVertexComparer::.cctor()
extern void BorderVertexComparer__cctor_m61DECFC4F7DFC4BC778B09807637F10C79A491FE (void);
// 0x000000D8 System.Void UnityMeshSimplifier.SimplificationOptions::.cctor()
extern void SimplificationOptions__cctor_m40C045953ACE182EB44B86C1EDE1091584A91FC4 (void);
// 0x000000D9 System.Double UnityMeshSimplifier.MathHelper::Min(System.Double,System.Double,System.Double)
extern void MathHelper_Min_m6A554506F737CB7AE3EFD88187F245E5329DFE7B (void);
// 0x000000DA System.Double UnityMeshSimplifier.MathHelper::Clamp(System.Double,System.Double,System.Double)
extern void MathHelper_Clamp_m30F196B0AA91BA9236E5397BE183FFEFE2E20647 (void);
// 0x000000DB System.Double UnityMeshSimplifier.MathHelper::TriangleArea(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void MathHelper_TriangleArea_mB391C6B2B2482D8D1128ACC6C2CBCA7E01ACA66C (void);
// 0x000000DC UnityEngine.Mesh UnityMeshSimplifier.MeshUtils::CreateMesh(UnityEngine.Vector3[],System.Int32[][],UnityEngine.Vector3[],UnityEngine.Vector4[],UnityEngine.Color[],UnityEngine.BoneWeight[],System.Collections.Generic.List`1<UnityEngine.Vector2>[],UnityEngine.Matrix4x4[],UnityMeshSimplifier.BlendShape[])
extern void MeshUtils_CreateMesh_m4991C1AB387FC8A66F0249180D88FD220E12F5F0 (void);
// 0x000000DD UnityEngine.Mesh UnityMeshSimplifier.MeshUtils::CreateMesh(UnityEngine.Vector3[],System.Int32[][],UnityEngine.Vector3[],UnityEngine.Vector4[],UnityEngine.Color[],UnityEngine.BoneWeight[],System.Collections.Generic.List`1<UnityEngine.Vector4>[],UnityEngine.Matrix4x4[],UnityMeshSimplifier.BlendShape[])
extern void MeshUtils_CreateMesh_m62341D7603CB99E194310805E1AB085517276875 (void);
// 0x000000DE UnityEngine.Mesh UnityMeshSimplifier.MeshUtils::CreateMesh(UnityEngine.Vector3[],System.Int32[][],UnityEngine.Vector3[],UnityEngine.Vector4[],UnityEngine.Color[],UnityEngine.BoneWeight[],System.Collections.Generic.List`1<UnityEngine.Vector2>[],System.Collections.Generic.List`1<UnityEngine.Vector3>[],System.Collections.Generic.List`1<UnityEngine.Vector4>[],UnityEngine.Matrix4x4[],UnityMeshSimplifier.BlendShape[])
extern void MeshUtils_CreateMesh_mD5EDC3D417AC008791F3A446E466A2BB2BD1E354 (void);
// 0x000000DF UnityMeshSimplifier.BlendShape[] UnityMeshSimplifier.MeshUtils::GetMeshBlendShapes(UnityEngine.Mesh)
extern void MeshUtils_GetMeshBlendShapes_m4E45CB44EFA01BD711B0DFAA58C9ED007A78F98B (void);
// 0x000000E0 System.Void UnityMeshSimplifier.MeshUtils::ApplyMeshBlendShapes(UnityEngine.Mesh,UnityMeshSimplifier.BlendShape[])
extern void MeshUtils_ApplyMeshBlendShapes_mD3D871C7A7D2DDA66BB1DDC5657EB0FE646C2DFF (void);
// 0x000000E1 System.Collections.Generic.List`1<UnityEngine.Vector4>[] UnityMeshSimplifier.MeshUtils::GetMeshUVs(UnityEngine.Mesh)
extern void MeshUtils_GetMeshUVs_m1BBA8C0B86F71951DAA5453F89261838E80C69F5 (void);
// 0x000000E2 System.Collections.Generic.List`1<UnityEngine.Vector4> UnityMeshSimplifier.MeshUtils::GetMeshUVs(UnityEngine.Mesh,System.Int32)
extern void MeshUtils_GetMeshUVs_m0573783F8A562F28636686DC19E63D95A43085BE (void);
// 0x000000E3 System.Int32 UnityMeshSimplifier.MeshUtils::GetUsedUVComponents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshUtils_GetUsedUVComponents_mB671B329B386BE0124296E9E8F9040E48C43EBBD (void);
// 0x000000E4 UnityEngine.Vector2[] UnityMeshSimplifier.MeshUtils::ConvertUVsTo2D(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshUtils_ConvertUVsTo2D_m86D202BA8ADD5106466D3724B0DEE77A0D1B2A5D (void);
// 0x000000E5 UnityEngine.Vector3[] UnityMeshSimplifier.MeshUtils::ConvertUVsTo3D(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void MeshUtils_ConvertUVsTo3D_mFE900B78FF6F8E19C5ECD8B6781CA4D0CEC133FE (void);
// 0x000000E6 System.Void UnityMeshSimplifier.MeshUtils::GetIndexMinMax(System.Int32[],System.Int32&,System.Int32&)
extern void MeshUtils_GetIndexMinMax_mFB4CCDC30CA1CF761CAD05578FB151694C74FA81 (void);
// 0x000000E7 System.Int32 UnityMeshSimplifier.ResizableArray`1::get_Length()
// 0x000000E8 T[] UnityMeshSimplifier.ResizableArray`1::get_Data()
// 0x000000E9 T UnityMeshSimplifier.ResizableArray`1::get_Item(System.Int32)
// 0x000000EA System.Void UnityMeshSimplifier.ResizableArray`1::set_Item(System.Int32,T)
// 0x000000EB System.Void UnityMeshSimplifier.ResizableArray`1::.ctor(System.Int32)
// 0x000000EC System.Void UnityMeshSimplifier.ResizableArray`1::.ctor(System.Int32,System.Int32)
// 0x000000ED System.Void UnityMeshSimplifier.ResizableArray`1::.ctor(T[])
// 0x000000EE System.Void UnityMeshSimplifier.ResizableArray`1::IncreaseCapacity(System.Int32)
// 0x000000EF System.Void UnityMeshSimplifier.ResizableArray`1::Clear()
// 0x000000F0 System.Void UnityMeshSimplifier.ResizableArray`1::Resize(System.Int32,System.Boolean)
// 0x000000F1 System.Void UnityMeshSimplifier.ResizableArray`1::TrimExcess()
// 0x000000F2 System.Void UnityMeshSimplifier.ResizableArray`1::Add(T)
// 0x000000F3 T[] UnityMeshSimplifier.ResizableArray`1::ToArray()
// 0x000000F4 System.Void UnityMeshSimplifier.ResizableArray`1::.cctor()
// 0x000000F5 System.Double UnityMeshSimplifier.SymmetricMatrix::get_Item(System.Int32)
extern void SymmetricMatrix_get_Item_mC468756241D3B38DBF98F8B3046A760BCA48B93E (void);
// 0x000000F6 System.Void UnityMeshSimplifier.SymmetricMatrix::.ctor(System.Double)
extern void SymmetricMatrix__ctor_mC816B4A0DC39FD8BD3ACA80B9DD7AC0DF23B64D6 (void);
// 0x000000F7 System.Void UnityMeshSimplifier.SymmetricMatrix::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
extern void SymmetricMatrix__ctor_m8E51AC041A36B6A2AB324AE4A24E8F20FA4574EC (void);
// 0x000000F8 System.Void UnityMeshSimplifier.SymmetricMatrix::.ctor(System.Double,System.Double,System.Double,System.Double)
extern void SymmetricMatrix__ctor_mB61BDC5A10C1F07E74461A88581CCD99F5AB16FA (void);
// 0x000000F9 UnityMeshSimplifier.SymmetricMatrix UnityMeshSimplifier.SymmetricMatrix::op_Addition(UnityMeshSimplifier.SymmetricMatrix,UnityMeshSimplifier.SymmetricMatrix)
extern void SymmetricMatrix_op_Addition_mADACB863304BEF8B16F7DFA84F3BDB164FD63C5A (void);
// 0x000000FA System.Double UnityMeshSimplifier.SymmetricMatrix::Determinant1()
extern void SymmetricMatrix_Determinant1_m0E744A899308253F25FF14197345B3CC1865697F (void);
// 0x000000FB System.Double UnityMeshSimplifier.SymmetricMatrix::Determinant2()
extern void SymmetricMatrix_Determinant2_mA29F78167D801DC2EA8CE0AD230DD68A52DEDF0F (void);
// 0x000000FC System.Double UnityMeshSimplifier.SymmetricMatrix::Determinant3()
extern void SymmetricMatrix_Determinant3_m8175B14C41E766E6819391AB76A63EC09727E17B (void);
// 0x000000FD System.Double UnityMeshSimplifier.SymmetricMatrix::Determinant4()
extern void SymmetricMatrix_Determinant4_m208FF8108D5146B438FF19B8815153F7933AD92A (void);
// 0x000000FE System.Double UnityMeshSimplifier.SymmetricMatrix::Determinant(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SymmetricMatrix_Determinant_m1878ED13220169D55178A862DECA232FA5EBF83D (void);
// 0x000000FF System.Double UnityMeshSimplifier.Vector3d::get_Magnitude()
extern void Vector3d_get_Magnitude_mBA233FFF6DBC6CEC28D041DDFD666B2E0EE61F67 (void);
// 0x00000100 System.Double UnityMeshSimplifier.Vector3d::get_MagnitudeSqr()
extern void Vector3d_get_MagnitudeSqr_mBDB076303CADC61E60615B64E2768383B9C422A0 (void);
// 0x00000101 UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::get_Normalized()
extern void Vector3d_get_Normalized_m9F6080E1F45AED81705593D5F84070F3EFA23FA6 (void);
// 0x00000102 System.Double UnityMeshSimplifier.Vector3d::get_Item(System.Int32)
extern void Vector3d_get_Item_m6D6CCEC8D6406B2C03D0EEECC069C34E72C39DE6 (void);
// 0x00000103 System.Void UnityMeshSimplifier.Vector3d::set_Item(System.Int32,System.Double)
extern void Vector3d_set_Item_mEFB86159BCCAFF368404732BD5F00D876DBBA4B9 (void);
// 0x00000104 System.Void UnityMeshSimplifier.Vector3d::.ctor(System.Double)
extern void Vector3d__ctor_m7344E0A9133884BA640103C308D71B492223C496 (void);
// 0x00000105 System.Void UnityMeshSimplifier.Vector3d::.ctor(System.Double,System.Double,System.Double)
extern void Vector3d__ctor_mCFEFA33C5B4FDF6DCD4CBD3F2DA069EEE9B339C7 (void);
// 0x00000106 System.Void UnityMeshSimplifier.Vector3d::.ctor(UnityEngine.Vector3)
extern void Vector3d__ctor_m812D203678E109C056CD9DEE9564085DCE3B7278 (void);
// 0x00000107 UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Addition(UnityMeshSimplifier.Vector3d,UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Addition_m2FCB8F0464D0F89C4988B76DC2A85384E948C1E1 (void);
// 0x00000108 UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Subtraction(UnityMeshSimplifier.Vector3d,UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Subtraction_mA12D56F0EF4CF80D49AE25D08CC81F283551081F (void);
// 0x00000109 UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Multiply(UnityMeshSimplifier.Vector3d,System.Double)
extern void Vector3d_op_Multiply_m317956AC532AE9DA01EC37264CE197B2492BBAFB (void);
// 0x0000010A UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Multiply(System.Double,UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Multiply_mB64705FB1C6C206100C1D17E4E25182E0BC44149 (void);
// 0x0000010B UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Division(UnityMeshSimplifier.Vector3d,System.Double)
extern void Vector3d_op_Division_m3CCAF456CAA6BE5335919F917356C2D006797506 (void);
// 0x0000010C UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_UnaryNegation(UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_UnaryNegation_mD1C3B958A197539E109A85A709E5CFADBBA8725D (void);
// 0x0000010D System.Boolean UnityMeshSimplifier.Vector3d::op_Equality(UnityMeshSimplifier.Vector3d,UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Equality_m1832F2D54BB28BE6454CC089B2195731C9FA3A7A (void);
// 0x0000010E System.Boolean UnityMeshSimplifier.Vector3d::op_Inequality(UnityMeshSimplifier.Vector3d,UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Inequality_mE8D31EF17303281237D8A1BFE57262ADDD4EECB6 (void);
// 0x0000010F UnityMeshSimplifier.Vector3d UnityMeshSimplifier.Vector3d::op_Implicit(UnityEngine.Vector3)
extern void Vector3d_op_Implicit_mE5B4EDEB424971F5E4ACBCF7B970F8C12ED5AFC2 (void);
// 0x00000110 UnityEngine.Vector3 UnityMeshSimplifier.Vector3d::op_Explicit(UnityMeshSimplifier.Vector3d)
extern void Vector3d_op_Explicit_mE1149E719836DB91EB1EA869B3D2E8B6FECCB569 (void);
// 0x00000111 System.Void UnityMeshSimplifier.Vector3d::Set(System.Double,System.Double,System.Double)
extern void Vector3d_Set_mAA0434EBEDFF9508E5021421C5AC32B0FE11B0B0 (void);
// 0x00000112 System.Void UnityMeshSimplifier.Vector3d::Scale(UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Scale_mBB237AAF53FB760F6A6CE4434C5B16FF7E220B4E (void);
// 0x00000113 System.Void UnityMeshSimplifier.Vector3d::Normalize()
extern void Vector3d_Normalize_m6B367A95C64AE003303878734DF3FF107011B47B (void);
// 0x00000114 System.Void UnityMeshSimplifier.Vector3d::Clamp(System.Double,System.Double)
extern void Vector3d_Clamp_mCB9B1BD8777B3F1E2FF7A44F5D1FB4F39F4BC3CD (void);
// 0x00000115 System.Int32 UnityMeshSimplifier.Vector3d::GetHashCode()
extern void Vector3d_GetHashCode_mAB08A402C0AAE2CC7813FECB24FA4D1C24E3A51F (void);
// 0x00000116 System.Boolean UnityMeshSimplifier.Vector3d::Equals(System.Object)
extern void Vector3d_Equals_mB76ED85E724B29056023F98659B97D55F422FC0E (void);
// 0x00000117 System.Boolean UnityMeshSimplifier.Vector3d::Equals(UnityMeshSimplifier.Vector3d)
extern void Vector3d_Equals_m792248AEDB234E182A3DF60F80B56A47363E8D71 (void);
// 0x00000118 System.String UnityMeshSimplifier.Vector3d::ToString()
extern void Vector3d_ToString_m25C3E5B56DB8C4DCBB4D6D2B312DFCB0589BC74F (void);
// 0x00000119 System.String UnityMeshSimplifier.Vector3d::ToString(System.String)
extern void Vector3d_ToString_mA0865B708213DD4B6259466979593C738EE802B7 (void);
// 0x0000011A System.Double UnityMeshSimplifier.Vector3d::Dot(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Dot_m6998B1D80E242EB5C1A5ACCD4D5970C263E5D715 (void);
// 0x0000011B System.Void UnityMeshSimplifier.Vector3d::Cross(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Cross_m957D97BFC93635123945F1F4632A5B0BE28F89FB (void);
// 0x0000011C System.Double UnityMeshSimplifier.Vector3d::Angle(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Angle_m2A5FAF591E5F55CE05F011EFD07593F7F6046AC0 (void);
// 0x0000011D System.Void UnityMeshSimplifier.Vector3d::Lerp(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,System.Double,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Lerp_m18887EC40F6088C5AD3C82C12F733B5808D29391 (void);
// 0x0000011E System.Void UnityMeshSimplifier.Vector3d::Scale(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Scale_mA9D25E0A90CAC6AD4AA8457FEDF0A544DC7AD6E9 (void);
// 0x0000011F System.Void UnityMeshSimplifier.Vector3d::Normalize(UnityMeshSimplifier.Vector3d&,UnityMeshSimplifier.Vector3d&)
extern void Vector3d_Normalize_m3E98677FE3326E069286080E88B74F7A05F2353B (void);
// 0x00000120 System.Void UnityMeshSimplifier.Vector3d::.cctor()
extern void Vector3d__cctor_mE4AED939AA803361470BD9FD6BF4336552CCBE61 (void);
static Il2CppMethodPointer s_methodPointers[288] = 
{
	BlendShape__ctor_m3871790C2786D76805CDC5AD29DB19DA81B5AC4A,
	BlendShapeFrame__ctor_m45CEC892AD8D65A153FAAC0206A9469C0AF98145,
	LODBackupComponent_get_OriginalRenderers_m484DA92D2B4A199D50B8CDB65BDC0356256FA55D,
	LODBackupComponent_set_OriginalRenderers_m6385D2A75FD6617736FE4EE05968BFF2E3DD5262,
	LODBackupComponent__ctor_m981426563EF27695D5DA32DBF8F52CBAF7DF2544,
	LODGeneratorHelper_get_FadeMode_m5FD9BC3B1F350A0979CFC3639584C81C0904651E,
	LODGeneratorHelper_set_FadeMode_mA95C65DACBB0FFE92756CF67551E19D56683D1AD,
	LODGeneratorHelper_get_AnimateCrossFading_m61A39A584C4B01FEAAEF6B748723704ACD257019,
	LODGeneratorHelper_set_AnimateCrossFading_m6FFC768B0A25770DCF7A230F9DC7546D4CEF4BA4,
	LODGeneratorHelper_get_AutoCollectRenderers_m6802F78D2EF57341C33E8730619E5731AAB18FA1,
	LODGeneratorHelper_set_AutoCollectRenderers_m24391B8D5811A944DD4C9B8C0FB613B1B39F5D5A,
	LODGeneratorHelper_get_SimplificationOptions_mE40EE26172A078EDBB656095F65BF97B1B823A92,
	LODGeneratorHelper_set_SimplificationOptions_m0B31AAC1C138D96089624155928CDA15A94EAB44,
	LODGeneratorHelper_get_SaveAssetsPath_m65B35A8FB8328C25DB5BE7EB4D47244822C8C009,
	LODGeneratorHelper_set_SaveAssetsPath_m0C025B183274E15B29D3FD73DBA9A3B5800C5C14,
	LODGeneratorHelper_get_Levels_mEA112741384066B9BA84DF4EC5F264C2B5FA8C79,
	LODGeneratorHelper_set_Levels_m5E7E0026E04F089507663F8B7624E95A0AEBF182,
	LODGeneratorHelper_get_IsGenerated_mDD401919EE858EC5E2DFCBAEB80C8A5946D20BF6,
	LODGeneratorHelper_Reset_mAC6708F221035ADB74FD12E4CA5D2160C21AFB22,
	LODGeneratorHelper__ctor_m9C34426DF31BCF6D182690FA612443A2C2B01233,
	LODGenerator_GenerateLODs_mEC45CCA86CCC2795F61B2C4045296BB742B105C6,
	LODGenerator_GenerateLODs_mAD073DE852E5C015AC98CD5F808679453CD5C0E5,
	LODGenerator_GenerateLODs_m4C902CD913FE564F9BF01AD36EA8EBAAF495630B,
	LODGenerator_DestroyLODs_m8BDFE19317DC351A0E67176300368D5271E7EADA,
	LODGenerator_DestroyLODs_mD594B741B58DCE8FC4E9752A5585F36DD38D4586,
	LODGenerator_GetStaticRenderers_mF2A7EC0F35300A539F29149CBFB0D24CA0FC3E6D,
	LODGenerator_GetSkinnedRenderers_m51BE3A0AF175EE7BD675F892C4AD8CD97C16E610,
	LODGenerator_CombineStaticMeshes_mBCBFFB5CBA219C5A5D80DA211A84472CCBFD5A42,
	LODGenerator_CombineSkinnedMeshes_mE9AA423218EAA67A5A652BA9D8964C853A279A51,
	LODGenerator_ParentAndResetTransform_mCB3D6A8A23BECDF411DC44D2B13A482A57D71529,
	LODGenerator_ParentAndOffsetTransform_m237D9A2A40FF936598F15B614FF467010B8558A7,
	LODGenerator_CreateLevelRenderer_mDC383DC8038E953AE5CD45E8CA8D069976E4B676,
	LODGenerator_CreateSkinnedLevelRenderer_mD8C80E0F20DB7269315DE8C9E9377398D9D6739E,
	LODGenerator_FindBestRootBone_mFA39FCC06C589BCFEA1D9BE7460863F173714372,
	LODGenerator_SetupLevelRenderer_mC31214ECC3CAC955070530DCDFD256574A18B485,
	LODGenerator_GetChildRenderersForLOD_m62708B6553FBD8B08C7F4E68ACB09FCEE020589F,
	LODGenerator_CollectChildRenderersForLOD_m00122AE844566D484060F3B8A0CD7F193FA74853,
	LODGenerator_SimplifyMesh_m31626950F41CF3DE5E54C8A57B8556DF0FE43F71,
	LODGenerator_DestroyObject_m584BC0C074AF1998706F8AD1A90579D0FD6FC48B,
	LODGenerator_CreateBackup_m66A6B9FA7892EEAA2546C96C2C50D9B273EB617B,
	LODGenerator_RestoreBackup_m8BD1BD599E7B50B0745CE9E17BD6028DD824B602,
	LODGenerator_DestroyLODAssets_m4810581B075BD5BFDFD59F40B2CED8D75AEFDEE1,
	LODGenerator_DestroyLODMaterialAsset_mBA74ADB7F39982D3D5ECD9745BB49A1DCE870DA4,
	LODGenerator_DestroyLODAsset_m3A75EA93CF14C798DE2FD82DC14B6E1113F93DE4,
	LODGenerator_SaveLODMeshAsset_m005622DFE28FB6B7FCA0257CC28775EDAD7FB31D,
	LODGenerator_SaveAsset_m2CDD4DE21C8F6AEE700A9819F3879385532251E5,
	LODGenerator_CreateParentDirectory_m67D5A36916996F1281DC823842114EBDDDF74B19,
	LODGenerator_MakePathSafe_m7242E4619E3450DA70784FB7EDF59F72429AC92A,
	LODGenerator_ValidateSaveAssetsPath_m866B84040957B9BD075EABB64F81A32F0FAABF14,
	LODGenerator_DeleteEmptyDirectory_m88DBEACAB0D9165E6F111F7AF6965AAD7B9A5ADF,
	U3CU3Ec__cctor_mC7C00B3B0DF1073E1ACDEB2C1D82BB6AB88CD43C,
	U3CU3Ec__ctor_m11FBF4D3DB42A18211CAFB729EC6B64DF96FF47E,
	U3CU3Ec_U3CGenerateLODsU3Eb__6_0_m9B7F0722CF220551BC408F9C6478842C62979A50,
	U3CU3Ec_U3CGenerateLODsU3Eb__6_1_m92C7DB5AF7937FCDEE32E73D5FFEFFB3C2F3B7ED,
	U3CU3Ec_U3CGenerateLODsU3Eb__6_2_m7C0B8CE0A97AD9896082B83E2390F96AB5530775,
	U3CU3Ec_U3CGenerateLODsU3Eb__6_3_m6AB8926A919A160FF1451213A8648CC73ACCE93E,
	U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_0_mE03C73D720099B35FBFA9C655F348D13B0F3A179,
	U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_1_mE5A077B9CE40DD645EC50D52A333AE3B768C6488,
	U3CU3Ec_U3CCombineSkinnedMeshesU3Eb__12_2_mD58B8C1DFF878A6DD852010BD45A7A911A41837F,
	LODLevel_get_ScreenRelativeTransitionHeight_m5F0B45ABD745D1698A23AEA0F02BAFFDC1F29E65,
	LODLevel_set_ScreenRelativeTransitionHeight_mD1F80EEB33AA7CDA6E3DDCFE0EF2534D490AAE63,
	LODLevel_get_FadeTransitionWidth_mEC23EF5E34EF6356784C1A123D916829A2B906DC,
	LODLevel_set_FadeTransitionWidth_m739CA9CD6DB5D06F9FE785552FCE9A3006FF2CE7,
	LODLevel_get_Quality_m7DD83EEC2584A96168E12B5E6CB536F218898FF2,
	LODLevel_set_Quality_m207E02075FE35B9A43BB864846C82D6A8743373C,
	LODLevel_get_CombineMeshes_mB94F8EAF266789D367E056D4F49763A14679892A,
	LODLevel_set_CombineMeshes_m1F7F5638DEF0BB12D1AD22172150A27D57333809,
	LODLevel_get_CombineSubMeshes_mDDB737F80A03EBFA25576E2D0CC72536E91E8FAD,
	LODLevel_set_CombineSubMeshes_m2D31AC48566929B1E56C70932EDE4A8F7EA9BF0B,
	LODLevel_get_Renderers_m86529300B7C1872F627E70B429AEB5C51BC35735,
	LODLevel_set_Renderers_mF2FCF33531538CBD0B1FFA1EFD553E5BCE96B6C1,
	LODLevel_get_SkinQuality_mF8A2297264057180920DEEFD3EE46EDDCDA3213E,
	LODLevel_set_SkinQuality_m1794B41EFBA468CEECC262884AF7EABB02232567,
	LODLevel_get_ShadowCastingMode_mA0DDA97611108A166E805C73E202D97E9FEF225E,
	LODLevel_set_ShadowCastingMode_m38CFB075167A200EFE36221A2BD6591C842CDA1A,
	LODLevel_get_ReceiveShadows_m1438F60100335FDDB49A4A57347DB44294C6C184,
	LODLevel_set_ReceiveShadows_m8CF1D403BA211C5C92E40CA83FFFE68A4179ED10,
	LODLevel_get_MotionVectorGenerationMode_m15C482F7280CDA7F02B4A6494381EE37EE6CC941,
	LODLevel_set_MotionVectorGenerationMode_mDB12EA048CFD6BE27E6B014664B0028EB73C52EB,
	LODLevel_get_SkinnedMotionVectors_mAFAC1AF1AAF57F87C89C545559815901F99620C9,
	LODLevel_set_SkinnedMotionVectors_m859FDD4765F3D970A417B9D754EB9754070FAA95,
	LODLevel_get_LightProbeUsage_m53514686041B8FF574395D312CA9C50B177C7673,
	LODLevel_set_LightProbeUsage_m5265F48D485C6E25CA8502DBBC28859E22E50DB4,
	LODLevel_get_ReflectionProbeUsage_mCCD08D7BA5CF741FE718AD98002AC10730D8AF80,
	LODLevel_set_ReflectionProbeUsage_mE07C5D8A69773598EE6D7138DADB99A73FDE24B1,
	LODLevel__ctor_m83AF36B169F6391CCA9C9525A10E8112D5BBC83B,
	LODLevel__ctor_m1BCE73707FC8C4E53041818FC6EA9EA1B9FA93EC,
	LODLevel__ctor_m2A7EBCEF83EC7D7E89DBC8137D60BD0A562B29DE,
	MeshCombiner_CombineMeshes_mCF6219DA18AA1F32F9A0EB469D3152FB77CAAA30,
	MeshCombiner_CombineMeshes_mB270F6C283B18CDB5ACA1A26E431F47801F931D5,
	MeshCombiner_CombineMeshes_mB87158AC41F3DE9EE85D114D8B2F10B40CD232EC,
	MeshCombiner_CombineMeshes_m1179F48E91FA3016AF01779C0D85B285E331D73B,
	MeshCombiner_CopyVertexPositions_mBAC06F7623C2CE550A8B33F02F4343E16223C820,
	NULL,
	NULL,
	MeshCombiner_TransformVertices_m3D2F3E48FB093E5F1EB538DEA73031B04A33FB71,
	MeshCombiner_TransformNormals_m0436EB2E10CDF4434882A29F24E3CA4A5DED2C43,
	MeshCombiner_TransformTangents_m48D2A4696DCB89AD8E2127711C9E142B0F1763D4,
	MeshCombiner_TransformVertices_m21B26E4BE1D7EC98330A642BD3CD72DBC4F529A3,
	MeshCombiner_RemapBones_mE78155E50D07CAC92F7A6AD2D0429A532D7F8F7E,
	MeshCombiner_ScaleMatrix_mF7BD1663F7267C3EEC81B7B8626627889441858D,
	MeshSimplifier_get_PreserveBorders_m8C4FC4061BF1CC6B7321EE01E12734754340B9AF,
	MeshSimplifier_set_PreserveBorders_mC1C47FA150D6D2F75AF50F24C814D01E746705CD,
	MeshSimplifier_get_PreserveBorderEdges_mCC3DA5AB8FF239A1F66A41A90F861AE95A0D41BA,
	MeshSimplifier_set_PreserveBorderEdges_m44EADEC6EE08A2271AF515DBA2052450D6A867B1,
	MeshSimplifier_get_PreserveSeams_m39802B9C123D2ABCFA43ADE126AA4EB3B19C74A7,
	MeshSimplifier_set_PreserveSeams_m8175AE9BA05811AC1306144641DED96DE87FCC51,
	MeshSimplifier_get_PreserveUVSeamEdges_m1DB17F3C09F3AC704651F0C8A1400EF95C05E302,
	MeshSimplifier_set_PreserveUVSeamEdges_mA7C05E9E09256B050D672005F3081B6EA1C479EE,
	MeshSimplifier_get_PreserveFoldovers_m098FAF6D5AB415924618746A24F3E58E68A8F5A6,
	MeshSimplifier_set_PreserveFoldovers_mFFE2927DFA81B8F746794060CD7B2AAA21CA4CD3,
	MeshSimplifier_get_PreserveUVFoldoverEdges_m3BB059A4BC1F429A70446E3430061C3D85E8B477,
	MeshSimplifier_set_PreserveUVFoldoverEdges_mA56BD8D3CC60D2CB6C92D4A939F783EF0613C816,
	MeshSimplifier_get_EnableSmartLink_m0C0C218DE13678D7CAE30567B8111C8DD6310B49,
	MeshSimplifier_set_EnableSmartLink_mCEE7B775817B8578BFD0D65DFAA508B4602F174B,
	MeshSimplifier_get_MaxIterationCount_mF44B7EB8E333AD67E7BD21179CBCE8EB24B6B587,
	MeshSimplifier_set_MaxIterationCount_m3D4E7C2463B080BB228F88380B9AFA7E35DCD649,
	MeshSimplifier_get_Agressiveness_mEA3B0110264F8487F1F4FDBEE6B30D91A4A9BD1E,
	MeshSimplifier_set_Agressiveness_m0C9D2ED4021C08A690B58BFCA2011AD28C8B0CB6,
	MeshSimplifier_get_Verbose_mC2967C01E671DDD4E7F416D35736BA0659BAC6F2,
	MeshSimplifier_set_Verbose_m410F378A9CB49414184712D9ABCDD742FA05318C,
	MeshSimplifier_get_VertexLinkDistance_m72BFAD0F16B32F07A04F41A3837BBC438DAFE8A8,
	MeshSimplifier_set_VertexLinkDistance_m68DEC17A7CC7C436120C717E1A4BB3D836FE9708,
	MeshSimplifier_get_VertexLinkDistanceSqr_m695BC4BA12E89E9B4445E0F2A8DC73EBCD4C5EE0,
	MeshSimplifier_set_VertexLinkDistanceSqr_m8D7B10B31C909F26D0997DEE78F9434EC77840A7,
	MeshSimplifier_get_Vertices_mF63825865EA820837705885F1FA400995A7E33CE,
	MeshSimplifier_set_Vertices_mEA49F68B76E315B8C8222355DAD611E1A81A6B08,
	MeshSimplifier_get_SubMeshCount_m49DEBC4DB9CB5DF13A626781004432F81F05FB8C,
	MeshSimplifier_get_BlendShapeCount_m2385E18AF64C1C1F6ED43F2E8581003601026FBF,
	MeshSimplifier_get_Normals_m4010FF45261167E95780FD92E89A566A23D0D28B,
	MeshSimplifier_set_Normals_m5D544D2B7EE28E5428ED42FC25243EC542BC55FD,
	MeshSimplifier_get_Tangents_m42EB38FEBBBBBAE567D1766252D336482E975FF5,
	MeshSimplifier_set_Tangents_mD016AC9103F8C94AB96F8CE0F4C6ED99785C9E4D,
	MeshSimplifier_get_UV1_mC77A342C1E14A2786FA5E47D9A27FFB5C76545A7,
	MeshSimplifier_set_UV1_m908AFF8A57B0646769139BCFB82C55FB15772459,
	MeshSimplifier_get_UV2_m4516F296BE3AEB92DD7FA15EA6E819B75157A867,
	MeshSimplifier_set_UV2_mF6F525E5331826A10C6590D34387F99F5A623084,
	MeshSimplifier_get_UV3_mA200738AF386B7F674DF9BDDA3DE5E3FA68E5811,
	MeshSimplifier_set_UV3_m39214DD70A0EBB1222832CEA35E7F2F250BCC2EF,
	MeshSimplifier_get_UV4_mE3A040D906F56BE80966BCE46B47C83BA5A6914D,
	MeshSimplifier_set_UV4_m33C527408B06B96E9DE7D29C3B0191E34CB9E67E,
	MeshSimplifier_get_Colors_mAC7B2ADD361E3C8E2C9195FB532A71B271F6A4D1,
	MeshSimplifier_set_Colors_mD520933E0B454F5B3A959941574A365867C2EE29,
	MeshSimplifier_get_BoneWeights_m7CB0CB54F0BFFE9094B38D7594EE14815F342CF6,
	MeshSimplifier_set_BoneWeights_m6269DBDAF129ABC14DA57D6EEA25E3020963F363,
	MeshSimplifier__ctor_m8F671CF05AD7B4AB2299F93EE42BD8BCC4304FF6,
	MeshSimplifier__ctor_mDF5550C531DFA38AA35A82E26453BFE455D59039,
	NULL,
	MeshSimplifier_VertexError_m044186C81022374C27E83978B9E2B06F04914BA2,
	MeshSimplifier_CalculateError_m1BBBED9A2BABAABC00DFCBE9FD3C8956C1D3358B,
	MeshSimplifier_CalculateBarycentricCoords_m457252058905579CA49FFA5953480AC5F84A02D5,
	MeshSimplifier_NormalizeTangent_m09069F776466F1C3F53C340474D83AF2DC4F04CF,
	MeshSimplifier_Flipped_m4CE3ECB82844E1DCB0DEF0A4CD2EE75F1C87BF14,
	MeshSimplifier_UpdateTriangles_mC8FF0F2720577392C7F3A167ABDFDDCFF271C39F,
	MeshSimplifier_InterpolateVertexAttributes_mA64D9BB4B51F373A3A16F3FF6D132B73153A2F7D,
	MeshSimplifier_AreUVsTheSame_m45B80546BAE1FFC13B94057DC86310F57300EDF7,
	MeshSimplifier_RemoveVertexPass_m3FD80A7FA1E9933B0FAEAA5FC9B34D63C07903BF,
	MeshSimplifier_UpdateMesh_mFC2D87867499C208C52D4BD8E9D4E224BC23E524,
	MeshSimplifier_UpdateReferences_m9AD7EF93152C0F9DAB0AC62B2522BB14A67583F4,
	MeshSimplifier_CompactMesh_m246B6008FA9727B9900BEFC60E1EE85A7C7EE213,
	MeshSimplifier_CalculateSubMeshOffsets_m2767488A342E2D188E6830C05A0215EBED85540F,
	MeshSimplifier_GetAllSubMeshTriangles_m3865BB75CA531D49EA56831A979DDB5D3CD9B3FD,
	MeshSimplifier_GetSubMeshTriangles_mC796C504580391E60B9BB2A5E9545F25ECB4A92D,
	MeshSimplifier_ClearSubMeshes_m3B0D1D3B080630E6304362EE01F8C5C138BAE015,
	MeshSimplifier_AddSubMeshTriangles_m85C05C1AF2E7338F6ADC6138BD3259CAD0C0F441,
	MeshSimplifier_AddSubMeshTriangles_m5AF7025ABE70F303B48FA7727BC3A5BDCE23EA84,
	MeshSimplifier_GetUVs2D_m569776C010F7857FDA45A1964E427BB8FD19D48F,
	MeshSimplifier_GetUVs3D_m75B142FCF183B4CA043D4FA4F18B6677E7E03C88,
	MeshSimplifier_GetUVs4D_m98D9E49923F8D07B29A7F2015D1BC11B9EF9CBBC,
	MeshSimplifier_GetUVs_m5B3DDB579B25D0F0B2A88351CB9429B4CEAC0887,
	MeshSimplifier_GetUVs_mF2AE33305BE6F36E69F2B880092FDA9BD5465791,
	MeshSimplifier_GetUVs_m8AF3296700235E9776FA19E068D18D3CD1CDCDE5,
	MeshSimplifier_SetUVs_mC535FEE97DC218268D036E45ED0F94B32732AD84,
	MeshSimplifier_SetUVs_m50D2209B26BDE9888E3631BDD72D0533C25AF2FD,
	MeshSimplifier_SetUVs_mD1097423C57D12494C4D8DAAA3F3AAD6A4EBF2F1,
	MeshSimplifier_SetUVs_m7044D6D18F65BBC318A370A0A2AB3C4CE8C8ADC2,
	MeshSimplifier_SetUVs_m3215E2332B75F158E2324FD0A523DBE4CFA14E81,
	MeshSimplifier_SetUVs_mEA7225D12789927D11EDCEBBCEFB362835C11CA5,
	MeshSimplifier_SetUVsAuto_m23DD2015AFB94EE6AD2BCBFDA256795EEB97DF0D,
	MeshSimplifier_GetAllBlendShapes_mDA38116AA081E2B83ECE86F6D213EE7A0D6F9853,
	MeshSimplifier_GetBlendShape_mAD37153429C8D1C7421279991644E582CEC653A4,
	MeshSimplifier_ClearBlendShapes_mA6A7D610FAC2286DDE65DD071399C0504193BE5F,
	MeshSimplifier_AddBlendShape_m6F8DE7A349E01AF6D17378B2784CA0F14059C354,
	MeshSimplifier_AddBlendShapes_mC05996B1FF9F118CE995CE10851310A5CA52C6D4,
	MeshSimplifier_Initialize_mEDB0A96361FE766A8A3C921A05DFE25FEFF7CD01,
	MeshSimplifier_SimplifyMesh_mF645D1B4BB52C39374CA402085138E591D921A9A,
	MeshSimplifier_SimplifyMeshLossless_mCDD5FADACDC61B66F16297E65A9EE8759BCFC77D,
	MeshSimplifier_ToMesh_m5FFB789E8E7D40BBC473564D73629383FED01E0D,
	Triangle_get_Item_m8340531208756C9FA4D5F390799CAD4B0505D71C,
	Triangle_set_Item_m2FE750EB184609848FF49F65457727F79D3039D0,
	Triangle__ctor_mF9C562B7EE15038584BC7B7E2892CBDBB828CBBE,
	Triangle_GetAttributeIndices_mB36F4A82121F20D5E38A9A1674D53B8485FB64CE,
	Triangle_SetAttributeIndex_m386E04F01CD553230913A921065F818AB178319D,
	Triangle_GetErrors_m9005DCF223C7FF48287C8695753C3DFCF861BB59,
	Vertex__ctor_mCD04A9DCA2249D64D5D3CA9E4484FDA2201712FB,
	Ref_Set_mE9B966F214EA0F9A1FA0A69FFBC22DC159C5743F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BlendShapeContainer__ctor_m76AEDC83F0880C099B85148C586B70A25F612F27,
	BlendShapeContainer_MoveVertexElement_m5D14043CDF543A06DF4BEEEAC11366FE8AE38AEC,
	BlendShapeContainer_InterpolateVertexAttributes_mBC395CB54586B3FB92F292AF965B1F504DBB84CF,
	BlendShapeContainer_Resize_m21A42F9C7C06D74CEE89BB048E15638F4A899B63,
	BlendShapeContainer_ToBlendShape_mB726B1F9B05EABAA9B68F0897FAC6B15CAE265A1,
	BlendShapeFrameContainer__ctor_m9FA507F084341EF12A2DFD1F188A5AC6169361F5,
	BlendShapeFrameContainer_MoveVertexElement_m13A9EBD09F1154B6DACACF04A32C743B4C6020D4,
	BlendShapeFrameContainer_InterpolateVertexAttributes_m3594E774FCB789BE3DD3F086BFD16CAAD1812F32,
	BlendShapeFrameContainer_Resize_m2BD253A130FF46436BADCC2B266ACCB2CA2F81EE,
	BlendShapeFrameContainer_ToBlendShapeFrame_mA2731B1B58C7C8ADA6416FBE6D1C4BF183608133,
	BorderVertex__ctor_mAB9D595A3D6329170C044F31E9D8167C7C57E179,
	BorderVertexComparer_Compare_mA6795FE7D80BC4620DBD618CBC9CF12410951AE5,
	BorderVertexComparer__ctor_m650C577E446FA170F476DD597B960CDDD84A888C,
	BorderVertexComparer__cctor_m61DECFC4F7DFC4BC778B09807637F10C79A491FE,
	SimplificationOptions__cctor_m40C045953ACE182EB44B86C1EDE1091584A91FC4,
	MathHelper_Min_m6A554506F737CB7AE3EFD88187F245E5329DFE7B,
	MathHelper_Clamp_m30F196B0AA91BA9236E5397BE183FFEFE2E20647,
	MathHelper_TriangleArea_mB391C6B2B2482D8D1128ACC6C2CBCA7E01ACA66C,
	MeshUtils_CreateMesh_m4991C1AB387FC8A66F0249180D88FD220E12F5F0,
	MeshUtils_CreateMesh_m62341D7603CB99E194310805E1AB085517276875,
	MeshUtils_CreateMesh_mD5EDC3D417AC008791F3A446E466A2BB2BD1E354,
	MeshUtils_GetMeshBlendShapes_m4E45CB44EFA01BD711B0DFAA58C9ED007A78F98B,
	MeshUtils_ApplyMeshBlendShapes_mD3D871C7A7D2DDA66BB1DDC5657EB0FE646C2DFF,
	MeshUtils_GetMeshUVs_m1BBA8C0B86F71951DAA5453F89261838E80C69F5,
	MeshUtils_GetMeshUVs_m0573783F8A562F28636686DC19E63D95A43085BE,
	MeshUtils_GetUsedUVComponents_mB671B329B386BE0124296E9E8F9040E48C43EBBD,
	MeshUtils_ConvertUVsTo2D_m86D202BA8ADD5106466D3724B0DEE77A0D1B2A5D,
	MeshUtils_ConvertUVsTo3D_mFE900B78FF6F8E19C5ECD8B6781CA4D0CEC133FE,
	MeshUtils_GetIndexMinMax_mFB4CCDC30CA1CF761CAD05578FB151694C74FA81,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SymmetricMatrix_get_Item_mC468756241D3B38DBF98F8B3046A760BCA48B93E,
	SymmetricMatrix__ctor_mC816B4A0DC39FD8BD3ACA80B9DD7AC0DF23B64D6,
	SymmetricMatrix__ctor_m8E51AC041A36B6A2AB324AE4A24E8F20FA4574EC,
	SymmetricMatrix__ctor_mB61BDC5A10C1F07E74461A88581CCD99F5AB16FA,
	SymmetricMatrix_op_Addition_mADACB863304BEF8B16F7DFA84F3BDB164FD63C5A,
	SymmetricMatrix_Determinant1_m0E744A899308253F25FF14197345B3CC1865697F,
	SymmetricMatrix_Determinant2_mA29F78167D801DC2EA8CE0AD230DD68A52DEDF0F,
	SymmetricMatrix_Determinant3_m8175B14C41E766E6819391AB76A63EC09727E17B,
	SymmetricMatrix_Determinant4_m208FF8108D5146B438FF19B8815153F7933AD92A,
	SymmetricMatrix_Determinant_m1878ED13220169D55178A862DECA232FA5EBF83D,
	Vector3d_get_Magnitude_mBA233FFF6DBC6CEC28D041DDFD666B2E0EE61F67,
	Vector3d_get_MagnitudeSqr_mBDB076303CADC61E60615B64E2768383B9C422A0,
	Vector3d_get_Normalized_m9F6080E1F45AED81705593D5F84070F3EFA23FA6,
	Vector3d_get_Item_m6D6CCEC8D6406B2C03D0EEECC069C34E72C39DE6,
	Vector3d_set_Item_mEFB86159BCCAFF368404732BD5F00D876DBBA4B9,
	Vector3d__ctor_m7344E0A9133884BA640103C308D71B492223C496,
	Vector3d__ctor_mCFEFA33C5B4FDF6DCD4CBD3F2DA069EEE9B339C7,
	Vector3d__ctor_m812D203678E109C056CD9DEE9564085DCE3B7278,
	Vector3d_op_Addition_m2FCB8F0464D0F89C4988B76DC2A85384E948C1E1,
	Vector3d_op_Subtraction_mA12D56F0EF4CF80D49AE25D08CC81F283551081F,
	Vector3d_op_Multiply_m317956AC532AE9DA01EC37264CE197B2492BBAFB,
	Vector3d_op_Multiply_mB64705FB1C6C206100C1D17E4E25182E0BC44149,
	Vector3d_op_Division_m3CCAF456CAA6BE5335919F917356C2D006797506,
	Vector3d_op_UnaryNegation_mD1C3B958A197539E109A85A709E5CFADBBA8725D,
	Vector3d_op_Equality_m1832F2D54BB28BE6454CC089B2195731C9FA3A7A,
	Vector3d_op_Inequality_mE8D31EF17303281237D8A1BFE57262ADDD4EECB6,
	Vector3d_op_Implicit_mE5B4EDEB424971F5E4ACBCF7B970F8C12ED5AFC2,
	Vector3d_op_Explicit_mE1149E719836DB91EB1EA869B3D2E8B6FECCB569,
	Vector3d_Set_mAA0434EBEDFF9508E5021421C5AC32B0FE11B0B0,
	Vector3d_Scale_mBB237AAF53FB760F6A6CE4434C5B16FF7E220B4E,
	Vector3d_Normalize_m6B367A95C64AE003303878734DF3FF107011B47B,
	Vector3d_Clamp_mCB9B1BD8777B3F1E2FF7A44F5D1FB4F39F4BC3CD,
	Vector3d_GetHashCode_mAB08A402C0AAE2CC7813FECB24FA4D1C24E3A51F,
	Vector3d_Equals_mB76ED85E724B29056023F98659B97D55F422FC0E,
	Vector3d_Equals_m792248AEDB234E182A3DF60F80B56A47363E8D71,
	Vector3d_ToString_m25C3E5B56DB8C4DCBB4D6D2B312DFCB0589BC74F,
	Vector3d_ToString_mA0865B708213DD4B6259466979593C738EE802B7,
	Vector3d_Dot_m6998B1D80E242EB5C1A5ACCD4D5970C263E5D715,
	Vector3d_Cross_m957D97BFC93635123945F1F4632A5B0BE28F89FB,
	Vector3d_Angle_m2A5FAF591E5F55CE05F011EFD07593F7F6046AC0,
	Vector3d_Lerp_m18887EC40F6088C5AD3C82C12F733B5808D29391,
	Vector3d_Scale_mA9D25E0A90CAC6AD4AA8457FEDF0A544DC7AD6E9,
	Vector3d_Normalize_m3E98677FE3326E069286080E88B74F7A05F2353B,
	Vector3d__cctor_mE4AED939AA803361470BD9FD6BF4336552CCBE61,
};
extern void BlendShape__ctor_m3871790C2786D76805CDC5AD29DB19DA81B5AC4A_AdjustorThunk (void);
extern void BlendShapeFrame__ctor_m45CEC892AD8D65A153FAAC0206A9469C0AF98145_AdjustorThunk (void);
extern void LODLevel_get_ScreenRelativeTransitionHeight_m5F0B45ABD745D1698A23AEA0F02BAFFDC1F29E65_AdjustorThunk (void);
extern void LODLevel_set_ScreenRelativeTransitionHeight_mD1F80EEB33AA7CDA6E3DDCFE0EF2534D490AAE63_AdjustorThunk (void);
extern void LODLevel_get_FadeTransitionWidth_mEC23EF5E34EF6356784C1A123D916829A2B906DC_AdjustorThunk (void);
extern void LODLevel_set_FadeTransitionWidth_m739CA9CD6DB5D06F9FE785552FCE9A3006FF2CE7_AdjustorThunk (void);
extern void LODLevel_get_Quality_m7DD83EEC2584A96168E12B5E6CB536F218898FF2_AdjustorThunk (void);
extern void LODLevel_set_Quality_m207E02075FE35B9A43BB864846C82D6A8743373C_AdjustorThunk (void);
extern void LODLevel_get_CombineMeshes_mB94F8EAF266789D367E056D4F49763A14679892A_AdjustorThunk (void);
extern void LODLevel_set_CombineMeshes_m1F7F5638DEF0BB12D1AD22172150A27D57333809_AdjustorThunk (void);
extern void LODLevel_get_CombineSubMeshes_mDDB737F80A03EBFA25576E2D0CC72536E91E8FAD_AdjustorThunk (void);
extern void LODLevel_set_CombineSubMeshes_m2D31AC48566929B1E56C70932EDE4A8F7EA9BF0B_AdjustorThunk (void);
extern void LODLevel_get_Renderers_m86529300B7C1872F627E70B429AEB5C51BC35735_AdjustorThunk (void);
extern void LODLevel_set_Renderers_mF2FCF33531538CBD0B1FFA1EFD553E5BCE96B6C1_AdjustorThunk (void);
extern void LODLevel_get_SkinQuality_mF8A2297264057180920DEEFD3EE46EDDCDA3213E_AdjustorThunk (void);
extern void LODLevel_set_SkinQuality_m1794B41EFBA468CEECC262884AF7EABB02232567_AdjustorThunk (void);
extern void LODLevel_get_ShadowCastingMode_mA0DDA97611108A166E805C73E202D97E9FEF225E_AdjustorThunk (void);
extern void LODLevel_set_ShadowCastingMode_m38CFB075167A200EFE36221A2BD6591C842CDA1A_AdjustorThunk (void);
extern void LODLevel_get_ReceiveShadows_m1438F60100335FDDB49A4A57347DB44294C6C184_AdjustorThunk (void);
extern void LODLevel_set_ReceiveShadows_m8CF1D403BA211C5C92E40CA83FFFE68A4179ED10_AdjustorThunk (void);
extern void LODLevel_get_MotionVectorGenerationMode_m15C482F7280CDA7F02B4A6494381EE37EE6CC941_AdjustorThunk (void);
extern void LODLevel_set_MotionVectorGenerationMode_mDB12EA048CFD6BE27E6B014664B0028EB73C52EB_AdjustorThunk (void);
extern void LODLevel_get_SkinnedMotionVectors_mAFAC1AF1AAF57F87C89C545559815901F99620C9_AdjustorThunk (void);
extern void LODLevel_set_SkinnedMotionVectors_m859FDD4765F3D970A417B9D754EB9754070FAA95_AdjustorThunk (void);
extern void LODLevel_get_LightProbeUsage_m53514686041B8FF574395D312CA9C50B177C7673_AdjustorThunk (void);
extern void LODLevel_set_LightProbeUsage_m5265F48D485C6E25CA8502DBBC28859E22E50DB4_AdjustorThunk (void);
extern void LODLevel_get_ReflectionProbeUsage_mCCD08D7BA5CF741FE718AD98002AC10730D8AF80_AdjustorThunk (void);
extern void LODLevel_set_ReflectionProbeUsage_mE07C5D8A69773598EE6D7138DADB99A73FDE24B1_AdjustorThunk (void);
extern void LODLevel__ctor_m83AF36B169F6391CCA9C9525A10E8112D5BBC83B_AdjustorThunk (void);
extern void LODLevel__ctor_m1BCE73707FC8C4E53041818FC6EA9EA1B9FA93EC_AdjustorThunk (void);
extern void LODLevel__ctor_m2A7EBCEF83EC7D7E89DBC8137D60BD0A562B29DE_AdjustorThunk (void);
extern void Triangle_get_Item_m8340531208756C9FA4D5F390799CAD4B0505D71C_AdjustorThunk (void);
extern void Triangle_set_Item_m2FE750EB184609848FF49F65457727F79D3039D0_AdjustorThunk (void);
extern void Triangle__ctor_mF9C562B7EE15038584BC7B7E2892CBDBB828CBBE_AdjustorThunk (void);
extern void Triangle_GetAttributeIndices_mB36F4A82121F20D5E38A9A1674D53B8485FB64CE_AdjustorThunk (void);
extern void Triangle_SetAttributeIndex_m386E04F01CD553230913A921065F818AB178319D_AdjustorThunk (void);
extern void Triangle_GetErrors_m9005DCF223C7FF48287C8695753C3DFCF861BB59_AdjustorThunk (void);
extern void Vertex__ctor_mCD04A9DCA2249D64D5D3CA9E4484FDA2201712FB_AdjustorThunk (void);
extern void Ref_Set_mE9B966F214EA0F9A1FA0A69FFBC22DC159C5743F_AdjustorThunk (void);
extern void BorderVertex__ctor_mAB9D595A3D6329170C044F31E9D8167C7C57E179_AdjustorThunk (void);
extern void SymmetricMatrix_get_Item_mC468756241D3B38DBF98F8B3046A760BCA48B93E_AdjustorThunk (void);
extern void SymmetricMatrix__ctor_mC816B4A0DC39FD8BD3ACA80B9DD7AC0DF23B64D6_AdjustorThunk (void);
extern void SymmetricMatrix__ctor_m8E51AC041A36B6A2AB324AE4A24E8F20FA4574EC_AdjustorThunk (void);
extern void SymmetricMatrix__ctor_mB61BDC5A10C1F07E74461A88581CCD99F5AB16FA_AdjustorThunk (void);
extern void SymmetricMatrix_Determinant1_m0E744A899308253F25FF14197345B3CC1865697F_AdjustorThunk (void);
extern void SymmetricMatrix_Determinant2_mA29F78167D801DC2EA8CE0AD230DD68A52DEDF0F_AdjustorThunk (void);
extern void SymmetricMatrix_Determinant3_m8175B14C41E766E6819391AB76A63EC09727E17B_AdjustorThunk (void);
extern void SymmetricMatrix_Determinant4_m208FF8108D5146B438FF19B8815153F7933AD92A_AdjustorThunk (void);
extern void SymmetricMatrix_Determinant_m1878ED13220169D55178A862DECA232FA5EBF83D_AdjustorThunk (void);
extern void Vector3d_get_Magnitude_mBA233FFF6DBC6CEC28D041DDFD666B2E0EE61F67_AdjustorThunk (void);
extern void Vector3d_get_MagnitudeSqr_mBDB076303CADC61E60615B64E2768383B9C422A0_AdjustorThunk (void);
extern void Vector3d_get_Normalized_m9F6080E1F45AED81705593D5F84070F3EFA23FA6_AdjustorThunk (void);
extern void Vector3d_get_Item_m6D6CCEC8D6406B2C03D0EEECC069C34E72C39DE6_AdjustorThunk (void);
extern void Vector3d_set_Item_mEFB86159BCCAFF368404732BD5F00D876DBBA4B9_AdjustorThunk (void);
extern void Vector3d__ctor_m7344E0A9133884BA640103C308D71B492223C496_AdjustorThunk (void);
extern void Vector3d__ctor_mCFEFA33C5B4FDF6DCD4CBD3F2DA069EEE9B339C7_AdjustorThunk (void);
extern void Vector3d__ctor_m812D203678E109C056CD9DEE9564085DCE3B7278_AdjustorThunk (void);
extern void Vector3d_Set_mAA0434EBEDFF9508E5021421C5AC32B0FE11B0B0_AdjustorThunk (void);
extern void Vector3d_Scale_mBB237AAF53FB760F6A6CE4434C5B16FF7E220B4E_AdjustorThunk (void);
extern void Vector3d_Normalize_m6B367A95C64AE003303878734DF3FF107011B47B_AdjustorThunk (void);
extern void Vector3d_Clamp_mCB9B1BD8777B3F1E2FF7A44F5D1FB4F39F4BC3CD_AdjustorThunk (void);
extern void Vector3d_GetHashCode_mAB08A402C0AAE2CC7813FECB24FA4D1C24E3A51F_AdjustorThunk (void);
extern void Vector3d_Equals_mB76ED85E724B29056023F98659B97D55F422FC0E_AdjustorThunk (void);
extern void Vector3d_Equals_m792248AEDB234E182A3DF60F80B56A47363E8D71_AdjustorThunk (void);
extern void Vector3d_ToString_m25C3E5B56DB8C4DCBB4D6D2B312DFCB0589BC74F_AdjustorThunk (void);
extern void Vector3d_ToString_mA0865B708213DD4B6259466979593C738EE802B7_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[66] = 
{
	{ 0x06000001, BlendShape__ctor_m3871790C2786D76805CDC5AD29DB19DA81B5AC4A_AdjustorThunk },
	{ 0x06000002, BlendShapeFrame__ctor_m45CEC892AD8D65A153FAAC0206A9469C0AF98145_AdjustorThunk },
	{ 0x0600003C, LODLevel_get_ScreenRelativeTransitionHeight_m5F0B45ABD745D1698A23AEA0F02BAFFDC1F29E65_AdjustorThunk },
	{ 0x0600003D, LODLevel_set_ScreenRelativeTransitionHeight_mD1F80EEB33AA7CDA6E3DDCFE0EF2534D490AAE63_AdjustorThunk },
	{ 0x0600003E, LODLevel_get_FadeTransitionWidth_mEC23EF5E34EF6356784C1A123D916829A2B906DC_AdjustorThunk },
	{ 0x0600003F, LODLevel_set_FadeTransitionWidth_m739CA9CD6DB5D06F9FE785552FCE9A3006FF2CE7_AdjustorThunk },
	{ 0x06000040, LODLevel_get_Quality_m7DD83EEC2584A96168E12B5E6CB536F218898FF2_AdjustorThunk },
	{ 0x06000041, LODLevel_set_Quality_m207E02075FE35B9A43BB864846C82D6A8743373C_AdjustorThunk },
	{ 0x06000042, LODLevel_get_CombineMeshes_mB94F8EAF266789D367E056D4F49763A14679892A_AdjustorThunk },
	{ 0x06000043, LODLevel_set_CombineMeshes_m1F7F5638DEF0BB12D1AD22172150A27D57333809_AdjustorThunk },
	{ 0x06000044, LODLevel_get_CombineSubMeshes_mDDB737F80A03EBFA25576E2D0CC72536E91E8FAD_AdjustorThunk },
	{ 0x06000045, LODLevel_set_CombineSubMeshes_m2D31AC48566929B1E56C70932EDE4A8F7EA9BF0B_AdjustorThunk },
	{ 0x06000046, LODLevel_get_Renderers_m86529300B7C1872F627E70B429AEB5C51BC35735_AdjustorThunk },
	{ 0x06000047, LODLevel_set_Renderers_mF2FCF33531538CBD0B1FFA1EFD553E5BCE96B6C1_AdjustorThunk },
	{ 0x06000048, LODLevel_get_SkinQuality_mF8A2297264057180920DEEFD3EE46EDDCDA3213E_AdjustorThunk },
	{ 0x06000049, LODLevel_set_SkinQuality_m1794B41EFBA468CEECC262884AF7EABB02232567_AdjustorThunk },
	{ 0x0600004A, LODLevel_get_ShadowCastingMode_mA0DDA97611108A166E805C73E202D97E9FEF225E_AdjustorThunk },
	{ 0x0600004B, LODLevel_set_ShadowCastingMode_m38CFB075167A200EFE36221A2BD6591C842CDA1A_AdjustorThunk },
	{ 0x0600004C, LODLevel_get_ReceiveShadows_m1438F60100335FDDB49A4A57347DB44294C6C184_AdjustorThunk },
	{ 0x0600004D, LODLevel_set_ReceiveShadows_m8CF1D403BA211C5C92E40CA83FFFE68A4179ED10_AdjustorThunk },
	{ 0x0600004E, LODLevel_get_MotionVectorGenerationMode_m15C482F7280CDA7F02B4A6494381EE37EE6CC941_AdjustorThunk },
	{ 0x0600004F, LODLevel_set_MotionVectorGenerationMode_mDB12EA048CFD6BE27E6B014664B0028EB73C52EB_AdjustorThunk },
	{ 0x06000050, LODLevel_get_SkinnedMotionVectors_mAFAC1AF1AAF57F87C89C545559815901F99620C9_AdjustorThunk },
	{ 0x06000051, LODLevel_set_SkinnedMotionVectors_m859FDD4765F3D970A417B9D754EB9754070FAA95_AdjustorThunk },
	{ 0x06000052, LODLevel_get_LightProbeUsage_m53514686041B8FF574395D312CA9C50B177C7673_AdjustorThunk },
	{ 0x06000053, LODLevel_set_LightProbeUsage_m5265F48D485C6E25CA8502DBBC28859E22E50DB4_AdjustorThunk },
	{ 0x06000054, LODLevel_get_ReflectionProbeUsage_mCCD08D7BA5CF741FE718AD98002AC10730D8AF80_AdjustorThunk },
	{ 0x06000055, LODLevel_set_ReflectionProbeUsage_mE07C5D8A69773598EE6D7138DADB99A73FDE24B1_AdjustorThunk },
	{ 0x06000056, LODLevel__ctor_m83AF36B169F6391CCA9C9525A10E8112D5BBC83B_AdjustorThunk },
	{ 0x06000057, LODLevel__ctor_m1BCE73707FC8C4E53041818FC6EA9EA1B9FA93EC_AdjustorThunk },
	{ 0x06000058, LODLevel__ctor_m2A7EBCEF83EC7D7E89DBC8137D60BD0A562B29DE_AdjustorThunk },
	{ 0x060000BD, Triangle_get_Item_m8340531208756C9FA4D5F390799CAD4B0505D71C_AdjustorThunk },
	{ 0x060000BE, Triangle_set_Item_m2FE750EB184609848FF49F65457727F79D3039D0_AdjustorThunk },
	{ 0x060000BF, Triangle__ctor_mF9C562B7EE15038584BC7B7E2892CBDBB828CBBE_AdjustorThunk },
	{ 0x060000C0, Triangle_GetAttributeIndices_mB36F4A82121F20D5E38A9A1674D53B8485FB64CE_AdjustorThunk },
	{ 0x060000C1, Triangle_SetAttributeIndex_m386E04F01CD553230913A921065F818AB178319D_AdjustorThunk },
	{ 0x060000C2, Triangle_GetErrors_m9005DCF223C7FF48287C8695753C3DFCF861BB59_AdjustorThunk },
	{ 0x060000C3, Vertex__ctor_mCD04A9DCA2249D64D5D3CA9E4484FDA2201712FB_AdjustorThunk },
	{ 0x060000C4, Ref_Set_mE9B966F214EA0F9A1FA0A69FFBC22DC159C5743F_AdjustorThunk },
	{ 0x060000D4, BorderVertex__ctor_mAB9D595A3D6329170C044F31E9D8167C7C57E179_AdjustorThunk },
	{ 0x060000F5, SymmetricMatrix_get_Item_mC468756241D3B38DBF98F8B3046A760BCA48B93E_AdjustorThunk },
	{ 0x060000F6, SymmetricMatrix__ctor_mC816B4A0DC39FD8BD3ACA80B9DD7AC0DF23B64D6_AdjustorThunk },
	{ 0x060000F7, SymmetricMatrix__ctor_m8E51AC041A36B6A2AB324AE4A24E8F20FA4574EC_AdjustorThunk },
	{ 0x060000F8, SymmetricMatrix__ctor_mB61BDC5A10C1F07E74461A88581CCD99F5AB16FA_AdjustorThunk },
	{ 0x060000FA, SymmetricMatrix_Determinant1_m0E744A899308253F25FF14197345B3CC1865697F_AdjustorThunk },
	{ 0x060000FB, SymmetricMatrix_Determinant2_mA29F78167D801DC2EA8CE0AD230DD68A52DEDF0F_AdjustorThunk },
	{ 0x060000FC, SymmetricMatrix_Determinant3_m8175B14C41E766E6819391AB76A63EC09727E17B_AdjustorThunk },
	{ 0x060000FD, SymmetricMatrix_Determinant4_m208FF8108D5146B438FF19B8815153F7933AD92A_AdjustorThunk },
	{ 0x060000FE, SymmetricMatrix_Determinant_m1878ED13220169D55178A862DECA232FA5EBF83D_AdjustorThunk },
	{ 0x060000FF, Vector3d_get_Magnitude_mBA233FFF6DBC6CEC28D041DDFD666B2E0EE61F67_AdjustorThunk },
	{ 0x06000100, Vector3d_get_MagnitudeSqr_mBDB076303CADC61E60615B64E2768383B9C422A0_AdjustorThunk },
	{ 0x06000101, Vector3d_get_Normalized_m9F6080E1F45AED81705593D5F84070F3EFA23FA6_AdjustorThunk },
	{ 0x06000102, Vector3d_get_Item_m6D6CCEC8D6406B2C03D0EEECC069C34E72C39DE6_AdjustorThunk },
	{ 0x06000103, Vector3d_set_Item_mEFB86159BCCAFF368404732BD5F00D876DBBA4B9_AdjustorThunk },
	{ 0x06000104, Vector3d__ctor_m7344E0A9133884BA640103C308D71B492223C496_AdjustorThunk },
	{ 0x06000105, Vector3d__ctor_mCFEFA33C5B4FDF6DCD4CBD3F2DA069EEE9B339C7_AdjustorThunk },
	{ 0x06000106, Vector3d__ctor_m812D203678E109C056CD9DEE9564085DCE3B7278_AdjustorThunk },
	{ 0x06000111, Vector3d_Set_mAA0434EBEDFF9508E5021421C5AC32B0FE11B0B0_AdjustorThunk },
	{ 0x06000112, Vector3d_Scale_mBB237AAF53FB760F6A6CE4434C5B16FF7E220B4E_AdjustorThunk },
	{ 0x06000113, Vector3d_Normalize_m6B367A95C64AE003303878734DF3FF107011B47B_AdjustorThunk },
	{ 0x06000114, Vector3d_Clamp_mCB9B1BD8777B3F1E2FF7A44F5D1FB4F39F4BC3CD_AdjustorThunk },
	{ 0x06000115, Vector3d_GetHashCode_mAB08A402C0AAE2CC7813FECB24FA4D1C24E3A51F_AdjustorThunk },
	{ 0x06000116, Vector3d_Equals_mB76ED85E724B29056023F98659B97D55F422FC0E_AdjustorThunk },
	{ 0x06000117, Vector3d_Equals_m792248AEDB234E182A3DF60F80B56A47363E8D71_AdjustorThunk },
	{ 0x06000118, Vector3d_ToString_m25C3E5B56DB8C4DCBB4D6D2B312DFCB0589BC74F_AdjustorThunk },
	{ 0x06000119, Vector3d_ToString_mA0865B708213DD4B6259466979593C738EE802B7_AdjustorThunk },
};
static const int32_t s_InvokerIndices[288] = 
{
	1627,
	627,
	3491,
	2922,
	3585,
	3469,
	2902,
	3429,
	2857,
	3429,
	2857,
	3527,
	2953,
	3491,
	2922,
	3491,
	2922,
	3429,
	3585,
	3585,
	5193,
	4135,
	3925,
	5058,
	5058,
	5193,
	5193,
	4407,
	4407,
	4958,
	4554,
	3766,
	3693,
	4810,
	4950,
	5193,
	4958,
	4415,
	5337,
	4958,
	5337,
	5337,
	5337,
	5337,
	3799,
	4958,
	5337,
	5193,
	5193,
	5058,
	5438,
	3585,
	2068,
	2583,
	2068,
	2583,
	2068,
	2068,
	2068,
	3528,
	2954,
	3528,
	2954,
	3528,
	2954,
	3429,
	2857,
	3429,
	2857,
	3491,
	2922,
	3469,
	2902,
	3469,
	2902,
	3429,
	2857,
	3469,
	2902,
	3429,
	2857,
	3469,
	2902,
	3469,
	2902,
	1656,
	321,
	177,
	4409,
	4134,
	4137,
	3764,
	4958,
	0,
	0,
	4950,
	4950,
	4950,
	4219,
	4958,
	4786,
	3429,
	2857,
	3429,
	2857,
	3429,
	2857,
	3429,
	2857,
	3429,
	2857,
	3429,
	2857,
	3429,
	2857,
	3469,
	2902,
	3445,
	2877,
	3429,
	2857,
	3445,
	2877,
	3445,
	2877,
	3491,
	2922,
	3469,
	3469,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3491,
	2922,
	3585,
	2922,
	0,
	352,
	700,
	3941,
	5328,
	184,
	282,
	284,
	655,
	146,
	2902,
	3585,
	3585,
	3585,
	3491,
	2581,
	3585,
	2922,
	2922,
	2581,
	2581,
	2581,
	1492,
	1492,
	1492,
	1492,
	1492,
	1492,
	1492,
	1492,
	1492,
	1492,
	3491,
	1829,
	3585,
	2853,
	2922,
	2922,
	2954,
	3585,
	3491,
	2419,
	1473,
	554,
	2922,
	1473,
	2922,
	2998,
	1473,
	0,
	0,
	0,
	0,
	0,
	2853,
	1473,
	284,
	1447,
	3425,
	2854,
	1473,
	284,
	1447,
	3426,
	1473,
	1199,
	3585,
	5438,
	5438,
	4313,
	4313,
	4311,
	3675,
	3675,
	3663,
	5193,
	4958,
	5193,
	4805,
	5136,
	5193,
	5193,
	4537,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2251,
	2877,
	10,
	549,
	4864,
	3445,
	3445,
	3445,
	3445,
	16,
	3445,
	3445,
	3577,
	2251,
	1460,
	2877,
	800,
	2997,
	4913,
	4913,
	4912,
	4911,
	4912,
	5323,
	4694,
	4694,
	5322,
	5320,
	800,
	2847,
	3585,
	1330,
	3469,
	2068,
	2154,
	3491,
	2583,
	4719,
	4477,
	4719,
	4168,
	4477,
	4923,
	5438,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000010, { 10, 5 } },
	{ 0x02000018, { 15, 5 } },
	{ 0x0600005E, { 0, 5 } },
	{ 0x0600005F, { 5, 1 } },
	{ 0x06000094, { 6, 4 } },
};
extern const uint32_t g_rgctx_Enumerable_Count_TisT_t1D5097073CDE6D99256D704B27D29830793A9900_m164B338F5289FCBC4CE5257BC6E9C42E06350C31;
extern const uint32_t g_rgctx_List_1_t5C2469F4A054140FD50DD51E9607B384AAA3DDC2;
extern const uint32_t g_rgctx_List_1_Add_mC591531AEA1B3DD58A2752CD2D1E0864AEF3E686;
extern const uint32_t g_rgctx_List_1__ctor_mF0A092BBF0809C7A2D483D24B6585CF2CA5313E2;
extern const uint32_t g_rgctx_List_1_AddRange_mE82DBAAD6A366BC5E969DE0CA2C850637DADE583;
extern const uint32_t g_rgctx_TU5BU5D_t474C19E058796536FE5DE117F2C044D2A8A3A83F;
extern const uint32_t g_rgctx_ResizableArray_1_t8D2A3AA878CA00C304036CBEFF0F64DE46066A90;
extern const uint32_t g_rgctx_ResizableArray_1__ctor_m8AF16F259586BEA49DE796EF2B4858F5395008BC;
extern const uint32_t g_rgctx_ResizableArray_1_Resize_m0BD36718F6E1E50DFB8E0B408B371C97EF2E180F;
extern const uint32_t g_rgctx_ResizableArray_1_get_Data_m2BB042F1BA808A30D75C3E1149D70B2FCD5E21B1;
extern const uint32_t g_rgctx_ResizableArray_1_tEAFFFD5AD43E1AD318A5C9F09FC08B38DC570612;
extern const uint32_t g_rgctx_ResizableArray_1_get_Data_m2B85CE0D6F547784CE0F2B73C73936718967131E;
extern const uint32_t g_rgctx_ResizableArray_1U5BU5D_t71F66273FC220A729B000235249FA42A0706480C;
extern const uint32_t g_rgctx_TVecU5BU5DU5BU5D_tD92D23C9D9A9177D6CFF86229F58222D8EB36649;
extern const uint32_t g_rgctx_ResizableArray_1_Resize_m09F8FA73459AD2EB126928C913680E30BC18B191;
extern const uint32_t g_rgctx_ResizableArray_1__ctor_m2BC19F7AD4A6EC3303CA8CED3450E26B2B5DF6DC;
extern const uint32_t g_rgctx_TU5BU5D_tCBA73A243CDE0EAB6E7AE94F242B782ECD38D491;
extern const uint32_t g_rgctx_ResizableArray_1_t384D7E5EB00703312FB7E22C95AC426135873C14;
extern const uint32_t g_rgctx_ResizableArray_1_IncreaseCapacity_mE052B9469A7AD3AC84E3B0D515DFB0B1DF03E2BD;
extern const uint32_t g_rgctx_ResizableArray_1_TrimExcess_m779FCB08674DECD55A451678B7C39CFD83BDD48E;
static const Il2CppRGCTXDefinition s_rgctxValues[20] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_Count_TisT_t1D5097073CDE6D99256D704B27D29830793A9900_m164B338F5289FCBC4CE5257BC6E9C42E06350C31 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t5C2469F4A054140FD50DD51E9607B384AAA3DDC2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mC591531AEA1B3DD58A2752CD2D1E0864AEF3E686 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mF0A092BBF0809C7A2D483D24B6585CF2CA5313E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_AddRange_mE82DBAAD6A366BC5E969DE0CA2C850637DADE583 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t474C19E058796536FE5DE117F2C044D2A8A3A83F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ResizableArray_1_t8D2A3AA878CA00C304036CBEFF0F64DE46066A90 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1__ctor_m8AF16F259586BEA49DE796EF2B4858F5395008BC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_Resize_m0BD36718F6E1E50DFB8E0B408B371C97EF2E180F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_get_Data_m2BB042F1BA808A30D75C3E1149D70B2FCD5E21B1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ResizableArray_1_tEAFFFD5AD43E1AD318A5C9F09FC08B38DC570612 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_get_Data_m2B85CE0D6F547784CE0F2B73C73936718967131E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ResizableArray_1U5BU5D_t71F66273FC220A729B000235249FA42A0706480C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TVecU5BU5DU5BU5D_tD92D23C9D9A9177D6CFF86229F58222D8EB36649 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_Resize_m09F8FA73459AD2EB126928C913680E30BC18B191 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1__ctor_m2BC19F7AD4A6EC3303CA8CED3450E26B2B5DF6DC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tCBA73A243CDE0EAB6E7AE94F242B782ECD38D491 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ResizableArray_1_t384D7E5EB00703312FB7E22C95AC426135873C14 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_IncreaseCapacity_mE052B9469A7AD3AC84E3B0D515DFB0B1DF03E2BD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ResizableArray_1_TrimExcess_m779FCB08674DECD55A451678B7C39CFD83BDD48E },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Whinarn_UnityMeshSimplifier_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Whinarn_UnityMeshSimplifier_Runtime_CodeGenModule = 
{
	"Whinarn.UnityMeshSimplifier.Runtime.dll",
	288,
	s_methodPointers,
	66,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	20,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
